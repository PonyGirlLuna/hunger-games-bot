Tested using OpenJDK 11

# Running the bot locally
- Create a directory for the bot to use as a working directory (should not be the project directory).
- Copy the `hunger-games-bot.properties` file into that directory.
- Edit that properties file and fill out the missing information.
- Copy the `pronouns.json` file into that directory.
- Copy the `data` folder in its entirety into that directory.
- Run `net.krazyweb.hungerbot.MainKt` and provide the properties file location via the `PROPERTIES_LOCATION` environment variable.

# Creating a bot account
See: https://javacord.org/wiki/essential-knowledge/creating-a-bot-account/

When the bot starts, an invite link will be logged. Open that link in a browser to invite the bot to your server.

# Running the bot as a service on Linux (Ubuntu)
- Create a user and group for the bot (this example will use `hungerbot` for both):
  - `useradd -M hungerbot`
  - `usermod -L hungerbot`
- Create a directory for the bot:
  - `mkdir /etc/hungerbot`
- Create a shell script in that new directory:
  - `touch /etc/hungerbot/hungerbot.sh`
  - `chmod +x /etc/hungerbot/hungerbot.sh`
  - `nano /etc/hungerbot/hungerbot.sh`
```
#!/bin/bash
export PROPERTIES_LOCATION=/etc/hungerbot/hungerbot.properties
/usr/bin/java -jar /etc/hungerbot/hungerbot.jar
```
- Copy the jar file for the bot to the bot's directory:
  - `cd /etc/hungerbot`
  - `cp <jarfile-with-dependencies> hungerbot.jar`
  - `chmod +x hungerbot.jar`
  - `chown hungerbot:hungerbot hungerbot.jar`
- Make the working directory:
  - `mkdir working`
- Copy the `data` directory to the bot's working directory:
  - `cp -R <data-dir> working/`
- Copy the `pronouns.json` file to the bot's working directory:
  - `cp <pronouns-file> working/`
- Set the ownership of all files in `/etc/hungerbot` to the `hungerbot` user/group:
  - `chown -R hungerbot:hungerbot /etc/hungerbot/`
- Create a service file:
  - `nano /etc/systemd/system/hungerbot.service`
```
[Unit]
Description=hungerbot

[Service]
User=hungerbot
WorkingDirectory=/etc/hungerbot/working
ExecStart=/etc/hungerbot/hungerbot.sh
Type=simple
SuccessExitStatus=0
Restart=on-failure
RestartSec=5s

[Install]
WantedBy=multi-user.target
```
- Enable the service:
  - `systemctl enable hungerbot`
- Start the service:
  - `systemctl start hungerbot`
- Monitor the logs for a successful start:
  - `tail -f /etc/hungerbot/working/hungerbot.log`
