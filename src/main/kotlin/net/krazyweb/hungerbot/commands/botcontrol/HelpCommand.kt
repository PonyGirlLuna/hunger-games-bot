package net.krazyweb.hungerbot.commands.botcontrol

import net.krazyweb.hungerbot.HungerGamesBot
import net.krazyweb.hungerbot.commands.Command
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.MessageBuilder

class HelpCommand(private val hungerGamesBot: HungerGamesBot) : Command("help", "usage", description = "Displays this help message") {

	override fun handleMessage(message: Message, messageContent: String) {

		var output = "${getReplyMention(message)} You can use the following commands:\n"

		hungerGamesBot.commands.filter { it.permissions(message) }.forEach {

			var commandOutput = it.aliases.joinToString(" or ", transform = { command -> "`.$command${if (it.arguments.isNotEmpty()) " ${it.arguments}" else ""}`" })
			commandOutput += " - ${it.description}\n"

			if ((output + commandOutput).length >= 2000) {
				MessageBuilder().append(output).send(message.channel)
				output = commandOutput
			} else {
				output += commandOutput
			}

		}

		MessageBuilder().append(output).send(message.channel)

	}

}
