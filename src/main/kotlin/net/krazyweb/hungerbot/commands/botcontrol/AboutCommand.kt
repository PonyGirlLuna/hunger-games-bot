package net.krazyweb.hungerbot.commands.botcontrol

import net.krazyweb.hungerbot.commands.Command
import net.krazyweb.hungerbot.getVersion
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.MessageBuilder

class AboutCommand : Command("about", description = "Displays information about this bot") {

	override fun handleMessage(message: Message, messageContent: String) {

		MessageBuilder().append("${getReplyMention(message)} This is **Hunger Bot ${getVersion()}**\n" +
				"This bot was created by KrazyTheFox. Source code can be found here: <https://gitlab.com/KrazyTheFox/hunger-games-bot>\n\n")
				.send(message.channel)

	}

}
