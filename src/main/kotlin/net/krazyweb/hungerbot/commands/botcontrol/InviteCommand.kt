package net.krazyweb.hungerbot.commands.botcontrol

import net.krazyweb.hungerbot.Permissions
import net.krazyweb.hungerbot.commands.Command
import net.krazyweb.hungerbot.createInvite
import net.krazyweb.util.BotProperties
import org.javacord.api.DiscordApi
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.MessageBuilder

class InviteCommand(val api: DiscordApi, val botProperties: BotProperties) : Command("invite", description = "Generates an invite link", permissions = Permissions.userId(botProperties.getLongProperty("superUserId"))) {

	override fun handleMessage(message: Message, messageContent: String) {
		MessageBuilder().append("${getReplyMention(message)} ${createInvite(api)}").send(message.channel)
	}

}
