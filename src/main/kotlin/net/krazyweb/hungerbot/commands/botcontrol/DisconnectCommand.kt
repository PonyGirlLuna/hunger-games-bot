package net.krazyweb.hungerbot.commands.botcontrol

import net.krazyweb.hungerbot.HungerGamesBot
import net.krazyweb.hungerbot.Permissions
import net.krazyweb.hungerbot.commands.Command
import net.krazyweb.util.BotProperties
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.MessageBuilder

class DisconnectCommand(val bot: HungerGamesBot, val botProperties: BotProperties) : Command("disconnect", "shutdown", "stop", description = "Stops the bot", permissions = Permissions.userId(botProperties.getLongProperty("superUserId"))) {

	override fun handleMessage(message: Message, messageContent: String) {

		MessageBuilder().append("${getReplyMention(message)} Shutting down.").send(message.channel)

		bot.stopBot()

	}

}
