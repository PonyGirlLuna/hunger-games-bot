package net.krazyweb.hungerbot.commands

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.javacord.api.entity.message.Message

abstract class Command(vararg val aliases: String, val arguments: String = "", val description: String, val permissions: (message: Message) -> Boolean = { true }) {

	private val log: Logger = LogManager.getLogger(Command::class.java)

	fun getReplyMention(message: Message): String {
		return "<@${message.author.id}>"
	}

	protected abstract fun handleMessage(message: Message, messageContent: String)

	fun delegateMessage(message: Message, messageContent: String) {
		Thread {
			try {
				handleMessage(message, messageContent)
			} catch (e: Exception) {
				log.error("Uncaught exception while processing command!", e)
			}
		}.start()
	}

}
