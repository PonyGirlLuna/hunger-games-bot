package net.krazyweb.hungerbot.commands.game

import net.krazyweb.hungerbot.HungerGamesBot
import net.krazyweb.hungerbot.Permissions
import net.krazyweb.hungerbot.commands.Command
import net.krazyweb.hungerbot.services.AutoGameService
import net.krazyweb.util.BotProperties
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.MessageBuilder

class AutoRunCommand(private val service: AutoGameService, private val bot: HungerGamesBot, val botProperties: BotProperties) : Command(
		"auto",
		description = "Enables or disables auto-play mode for an active game. Will automatically progress the gave every `<interval_in_seconds>` seconds (20 by default). Specify `start` or `stop` to select a mode. Use the command with no arguments to toggle between enabling and disabling the mode.",
		arguments = "<interval_in_seconds>` OR `<start/stop>",
		permissions = Permissions.role(botProperties.getLongProperty("gameHostRoleId"))
) {

	val log: Logger = LogManager.getLogger(AutoRunCommand::class.java)

	override fun handleMessage(message: Message, messageContent: String) {

		if (bot.activeGame == null) {
			MessageBuilder().setContent("There is no game in progress. Please start a new game before enabling auto mode.").send(message.channel)
			return
		}

		when {
			messageContent.isBlank() -> {
				service.running.set(!service.running.get())
			}
			messageContent.matches(Regex("^[0-9]{1,3}$")) -> {
				service.running.set(true)
				val time = messageContent.toInt()
				if (time >= 4) {
					service.intervalInSeconds.set(messageContent.toInt())
				} else {
					MessageBuilder().setContent("${getReplyMention(message)} A minimum of 4 seconds is required.").send(message.channel)
				}
			}
			messageContent.matches(Regex("^[0-9]{4,}$")) -> {
				MessageBuilder().setContent("${getReplyMention(message)} No no no no no. That's too long! D:").send(message.channel)
			}
			messageContent.toLowerCase() == "stop" -> {
				service.running.set(false)
			}
			messageContent.toLowerCase() == "start" -> {
				service.running.set(true)
			}
		}

		MessageBuilder().setContent(formatOutput()).send(message.channel)

	}

	private fun formatOutput(): String {

		if (!service.running.get()) {
			return "Automatic mode disabled."
		}

		return if (service.intervalInSeconds.get() == 1) {
			"Automatic mode enabled with an interval of 1 second."
		} else {
			"Automatic mode enabled with an interval of ${service.intervalInSeconds.get()} seconds."
		}

	}

}
