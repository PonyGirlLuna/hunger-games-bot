package net.krazyweb.hungerbot.commands.game

import net.krazyweb.hungerbot.Permissions
import net.krazyweb.hungerbot.commands.Command
import net.krazyweb.hungergames.data.PlayerPreferenceKey
import net.krazyweb.hungergames.services.PlayerPreferenceService
import net.krazyweb.util.BotProperties
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.MessageBuilder

class SetPreferenceCommand(botProperties: BotProperties, val playerPreferenceService: PlayerPreferenceService) : Command("preference", description = "Force sets/unsets a preference for a user. Use with care.", arguments = "<set/unset> <userId> <preference> <value>", permissions = Permissions.role(botProperties.getLongProperty("gameHostRoleId"))) {

	override fun handleMessage(message: Message, messageContent: String) {

		if (messageContent.startsWith("set")) {

			val split = messageContent.substringAfter("set ").split(Regex("\\s+"))
			val userId = split[0].toLong()
			val preference = PlayerPreferenceKey.valueOf(split[1].toUpperCase())
			val value = split.drop(2).joinToString(" ")

			playerPreferenceService.save(userId, preference, value)

			MessageBuilder().setContent("${getReplyMention(message)} Set `${preference.name}` to `$value` for user `$userId`.").send(message.channel)

		} else if (messageContent.startsWith("unset")) {

			val split = messageContent.substringAfter("unset ").split(Regex("\\s+"))
			val userId = split[0].toLong()
			val preference = PlayerPreferenceKey.valueOf(split[1].toUpperCase())

			playerPreferenceService.delete(userId, preference)

			MessageBuilder().setContent("${getReplyMention(message)} Deleted `${preference.name}` for user `$userId`.").send(message.channel)

		}

	}

}
