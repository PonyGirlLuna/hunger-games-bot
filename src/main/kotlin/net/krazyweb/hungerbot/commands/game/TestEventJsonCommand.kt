package net.krazyweb.hungerbot.commands.game

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.mockk.every
import io.mockk.mockk
import net.krazyweb.hungerbot.Permissions
import net.krazyweb.hungerbot.commands.Command
import net.krazyweb.hungergames.data.Conjugation
import net.krazyweb.hungergames.data.PlayerPreferenceKey
import net.krazyweb.hungergames.data.Pronouns
import net.krazyweb.hungergames.data.Tribute
import net.krazyweb.hungergames.data.events.Event
import net.krazyweb.hungergames.services.*
import net.krazyweb.util.BotProperties
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.MessageBuilder
import org.javacord.api.entity.message.embed.EmbedBuilder
import kotlin.random.Random

class TestEventJsonCommand(private val botProperties: BotProperties) : Command("test", description = "Tries to load and simulate a single event (in JSON form) using test data.", arguments = "<event>", permissions = Permissions.role(botProperties.getLongProperty("testerId"))) {

	val log: Logger = LogManager.getLogger(TestEventJsonCommand::class.java)

	private data class TestTribute(val name: String, val id: Long, val pronouns: String)
	private data class EventConditionSection(val title: String, val content: List<EventConditionField>)
	private data class EventConditionField(val title: String, val content: String?)

	override fun handleMessage(message: Message, messageContent: String) {

		val items = ItemLoaderService(FileService(botProperties)).loadAllItems()
		val testTributes = getTestTributes().map { Tribute(it.name, it.id, inventory = items.map { item -> item.clone() to 1000 }.toMap().toMutableMap()) }

		try {

			val event = EventLoaderService(getMockFileService(), getMockPronounService(), getMockPlayerPreferenceService()).loadEventArray("[$messageContent]").single()

			MessageBuilder().setEmbed(
					EmbedBuilder()
							.setTitle("Message Sample")
							.setDescription(event.runEvent(testTributes.shuffled(), items, Random(System.currentTimeMillis())))
			).send(message.channel)

			getConditions(event).forEach {

				log.info(it)

				var embed = EmbedBuilder().setTitle(it.title)
				it.content.forEach { eventConditionField ->
					embed = embed.addField(eventConditionField.title, eventConditionField.content)
				}

				MessageBuilder().setEmbed(embed).send(message.channel).get()

			}


		} catch (e: EventFormatException) {
			MessageBuilder().setContent("There was an error when processing your event: \n> ${e.message}").send(message.channel)
		} catch (e: Exception) {
			log.error(e, e)
			MessageBuilder().setContent("An unknown error occurred when trying to parse your event. Please double check your formatting. (${e.message})").send(message.channel)
		}

	}

	private fun getConditions(event: Event): List<EventConditionSection> {

		val output = mutableListOf<EventConditionSection>()

		event.apply {

			output += EventConditionSection("General", listOf(
					EventConditionField("Tributes Needed", getTotalNumberOfTributes().toString()),
					EventConditionField("Killed:", killed.map { it + 1 }.toString()),
					EventConditionField("Killers:", killers.map { it + 1 }.toString()),
					EventConditionField("Time of Day:", timeOfDay),
					EventConditionField("Arena Event:", "${this.event}"),
					EventConditionField("Death Type:", "$deathType")
			))

			val itemConditionFields = mutableListOf<EventConditionField>()

			if (itemReferences.any { it.owner != null }) {
				itemConditionFields += EventConditionField("Needed",
				itemReferences.filter { it.owner != null }.joinToString("\n") {
					"• `Tribute ${it.owner!! + 1}` needs ${it.quantity} item(s) with tag(s) [${it.tags.joinToString(",") { tag -> "`$tag`"}}]"
				})
			}

			if (itemReferences.any { it.owner == null }) {
				itemConditionFields += EventConditionField("Generated",
						itemReferences.filter { it.owner == null }.joinToString("\n") {
							"• ${it.quantity} item(s) with tag(s) [${it.tags.joinToString(",") { tag -> "`$tag`"}}]"
						})
			}

			if (inventoryChanges.isNotEmpty()) {
				itemConditionFields += EventConditionField("Inventory Changes",
				inventoryChanges.joinToString("\n") {
					val itemReference = itemReferences[it.item]
					if (it.from != null && it.to != null) {
						"• `Tribute ${it.from + 1}` **gives** `Tribute ${it.to + 1}` their ${itemReference.quantity} item(s) with tag(s) [${itemReference.tags.joinToString(",") { tag -> "`$tag`"}}]"
					} else if (it.from != null) {
						"• `Tribute ${it.from + 1}` **loses** ${itemReference.quantity} item(s) with tag(s) [${itemReference.tags.joinToString(",") { tag -> "`$tag`"}}]"
					} else if (it.to != null) {
						"• `Tribute ${it.to + 1}` **receives** ${itemReference.quantity} item(s) with tag(s) [${itemReference.tags.joinToString(",") { tag -> "`$tag`"}}]"
					} else {
						""
					}
				})
			}

			if (itemConditionFields.isNotEmpty()) {
				output += EventConditionSection("Items", itemConditionFields)
			}

		}

		return output

	}

	private fun getTestTributes(): List<TestTribute> {
		return jacksonObjectMapper().readValue(Thread.currentThread().contextClassLoader.getResourceAsStream("tributes.json"), object : TypeReference<List<TestTribute>>(){})
	}

	private fun getMockPlayerPreferenceService(): PlayerPreferenceService {

		val playerPreferenceService = mockk<PlayerPreferenceService>()
		val testTributes: List<TestTribute> = getTestTributes()

		testTributes.forEach {
			every { playerPreferenceService.get(it.id, PlayerPreferenceKey.PRONOUNS) } returns it.pronouns
		}

		return playerPreferenceService

	}

	private fun getMockPronounService(): PronounService {

		val pronounService = mockk<PronounService>()
		every { pronounService.getPronouns("he") } returns Pronouns("he", "he", "him", "his", "his", "himself", Conjugation.SINGULAR)
		every { pronounService.getPronouns("she") } returns Pronouns("she", "she", "her", "her", "hers", "herself", Conjugation.SINGULAR)
		every { pronounService.getPronouns("they") } returns Pronouns("they", "they", "them", "their", "theirs", "themselves", Conjugation.PLURAL)

		return pronounService

	}

	private fun getMockFileService(): FileService {
		val fileService = mockk<FileService>()
		every { fileService.readAllFromDirectory("data", "events") } returns emptyList()
		every { fileService.readAllFromDirectory("data", "events", "day") } returns emptyList()
		every { fileService.readAllFromDirectory("data", "events", "night") } returns emptyList()
		every { fileService.readAllFromDirectory("data", "events", "arena") } returns emptyList()
		return fileService
	}

}
