package net.krazyweb.hungerbot.commands.game

import net.krazyweb.hungerbot.HungerGamesBot
import net.krazyweb.hungerbot.Permissions
import net.krazyweb.hungerbot.commands.Command
import net.krazyweb.hungergames.game.BotGameEventSubscriber
import net.krazyweb.hungergames.game.TributeStatisticsSubscriber
import net.krazyweb.util.BotProperties
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.MessageBuilder

class ReplayCommand(val bot: HungerGamesBot, val botProperties: BotProperties) : Command("replay", description = "Replays the currently running game up to the current point. This will generate *a lot* of messages. Use with care!", permissions = Permissions.role(botProperties.getLongProperty("gameHostRoleId"))) {

	val log: Logger = LogManager.getLogger(ReplayCommand::class.java)

	override fun handleMessage(message: Message, messageContent: String) {

		if (bot.activeGame == null) {
			MessageBuilder().setContent("There is no game in progress. Cannot replay a game until one is loaded.").send(message.channel)
			return
		}

		bot.activeGame!!.removeSubscriber(bot.tributeStatisticsSubscriber!!)
		bot.activeGame!!.removeSubscriber(bot.botGameEventSubscriber!!)
		bot.tributeStatisticsSubscriber = TributeStatisticsSubscriber()
		bot.botGameEventSubscriber = BotGameEventSubscriber(bot, message.channel, botProperties)
		bot.activeGame!!.addSubscriber(bot.tributeStatisticsSubscriber!!)
		bot.activeGame!!.addSubscriber(bot.botGameEventSubscriber!!)
		bot.activeGame!!.replay()

		MessageBuilder().setContent("Caught up!").send(message.channel)

	}

}
