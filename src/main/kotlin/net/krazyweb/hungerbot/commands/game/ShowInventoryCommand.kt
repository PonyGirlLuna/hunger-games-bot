package net.krazyweb.hungerbot.commands.game

import net.krazyweb.hungerbot.HungerGamesBot
import net.krazyweb.hungerbot.commands.Command
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.MessageBuilder
import org.javacord.api.entity.message.embed.EmbedBuilder

class ShowInventoryCommand(private val bot: HungerGamesBot) : Command("stuff", "inventory", "items", description = "Shows your inventory. Add someone's `<name>` to the command to view that tribute's inventory instead.", arguments = "<name>") {

	override fun handleMessage(message: Message, messageContent: String) {

		if (bot.activeGame == null) {
			MessageBuilder().setContent("There is no game in progress. Please start a new game before trying to view an inventory.").send(message.channel)
			return
		}

		val messageAuthorId = message.userAuthor.get().id

		var output = ""
		val tribute = if (messageContent.isBlank()) {
				bot.activeGame!!.tributes.single { it.discordId == messageAuthorId }
		} else {
			if (message.mentionedUsers.isEmpty()) {
				bot.activeGame!!.tributes.single { it.name.toLowerCase() == messageContent.toLowerCase() }
			} else {
				bot.activeGame!!.tributes.single { it.discordId == message.mentionedUsers.first().id }
			}
		}

		var title = "**${tribute.name}** has the following items:"

		if (tribute.inventory.isNotEmpty()) {
			tribute.inventory.forEach {
				output += if (it.value == 1) {
					"• ${it.value} ${it.key.name}\n"
				} else {
					"• ${it.value} ${it.key.plural}\n"
				}
			}
		} else {
			title = "${tribute.name} has no items."
		}

		val embed = if (title.endsWith("no items.")) {
			EmbedBuilder()
					.setTitle(title)
		} else {
			EmbedBuilder()
					.setTitle(title)
					.setDescription(output)
		}

		MessageBuilder().setEmbed(embed).send(message.channel)

	}

}
