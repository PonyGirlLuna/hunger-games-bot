package net.krazyweb.hungerbot.commands.game

import net.krazyweb.hungerbot.commands.Command
import net.krazyweb.hungergames.data.PlayerPreferenceKey
import net.krazyweb.hungergames.services.PlayerPreferenceService
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.MessageBuilder

class UnsubscribeCommand(private val preferenceService: PlayerPreferenceService) : Command("unsubscribe", description = "Unsubscribe from game start notifications. The bot will no longer message you when a game has been started.") {

	val log: Logger = LogManager.getLogger(UnsubscribeCommand::class.java)

	override fun handleMessage(message: Message, messageContent: String) {

		message.userAuthor.ifPresent { user ->
			preferenceService.save(user.id, PlayerPreferenceKey.PING_ON_START, "false")
			MessageBuilder().setContent("${getReplyMention(message)} Got it. I'll stop notifying you when games start.").send(message.channel)
		}

	}

}
