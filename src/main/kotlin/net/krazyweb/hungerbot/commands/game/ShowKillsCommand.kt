package net.krazyweb.hungerbot.commands.game

import net.krazyweb.hungerbot.HungerGamesBot
import net.krazyweb.hungerbot.commands.Command
import net.krazyweb.hungergames.game.StatisticsKey
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.MessageBuilder
import org.javacord.api.entity.message.embed.EmbedBuilder

class ShowKillsCommand (private val bot: HungerGamesBot) : Command("kills", "kill", description = "Shows who the tribute killed, if any. Add someone's `<name>` to the command to view how many people that tribute has killed.", arguments = "<name>") {

	override fun handleMessage(message: Message, messageContent: String) {

		if (bot.activeGame == null) {
			MessageBuilder().setContent("There is no game in progress. Please start a new game before trying to view a tribute's kill count.").send(message.channel)
			return
		}
		
		val messageAuthorId = message.userAuthor.get().id

		val tribute = if (messageContent.isBlank()) {
				bot.activeGame!!.tributes.single { it.discordId == messageAuthorId }
		} else {
			if (message.mentionedUsers.isEmpty()) {
				bot.activeGame!!.tributes.single { it.name.toLowerCase() == messageContent.toLowerCase() }
			} else {
				bot.activeGame!!.tributes.single { it.discordId == message.mentionedUsers.first().id }
			}
		}

		val tributeStatistics = bot.tributeStatisticsSubscriber!!.getStatistics(tribute.discordId)
		val killCount = tributeStatistics.get(StatisticsKey.KILLS,0) as Int
		
		val output = when {
			killCount == 1 -> {
				"${tribute.name} has killed $killCount other player."
			}
			killCount > 1 -> {
				"${tribute.name} has killed $killCount other players."
			}
			else -> {
				"${tribute.name} has not killed anyone."
			}
		}

		/*val embed = if (title.endsWith("anyone.")) {
			EmbedBuilder()
					.setTitle(title)
		} else {*/
		val embed = EmbedBuilder()
		//			.setTitle(title)
					.setDescription(output)
		//}

		MessageBuilder().setEmbed(embed).send(message.channel)

	}

}
