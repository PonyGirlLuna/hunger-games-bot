package net.krazyweb.hungerbot.commands.game

import net.krazyweb.hungerbot.commands.Command
import net.krazyweb.hungergames.data.PlayerPreferenceKey
import net.krazyweb.hungergames.services.PlayerPreferenceService
import net.krazyweb.hungergames.services.PronounService
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.MessageBuilder

class PronounsPreferenceCommand(private val pronounService: PronounService, private val preferenceService: PlayerPreferenceService) : Command("pronouns", description = "Sets the pronouns the bot will use for you. Use without any arguments to see your current pronouns and the available options. To set your preferred pronouns, use `.pronouns <selection>`.", arguments = "<selection>") {

	val log: Logger = LogManager.getLogger(PronounsPreferenceCommand::class.java)

	override fun handleMessage(message: Message, messageContent: String) {

		val formattedSelection = messageContent.trim().toLowerCase()

		val pronouns = pronounService.getPronounList().filter { it.selectable }
		val nominativePronouns = pronouns.map { it.name }

		if (formattedSelection.isBlank()) {

			val userPronouns = pronounService.getPronouns(preferenceService.get(message.author.id, PlayerPreferenceKey.PRONOUNS))

			MessageBuilder().append("${getReplyMention(message)} I'm currently using the following pronouns for you: `${userPronouns.toReadableList()}`\n" +
					"To select a different pronoun, use `.pronouns <selection>`.\n" +
					"I currently support the following pronoun sets:\n" +
					"> ${nominativePronouns.joinToString("/")}\nIf there's pronouns that you would like to use and don't see them here, please message Krazy and he'll get them added for you.").send(message.channel).get()

		} else if (formattedSelection !in nominativePronouns) {

			MessageBuilder().append("${getReplyMention(message)} I'm sorry. I didn't recognize your pronoun selection of '$formattedSelection'. " +
					"I currently support the following pronoun sets:\n" +
					"> ${nominativePronouns.joinToString("/")}\nIf there's pronouns that you would like to use and don't see them here, please message Krazy and he'll get them added for you.").send(message.channel).get()

		} else {

			val selectedPronoun = pronouns.single { it.name == formattedSelection }

			preferenceService.save(message.author.id, PlayerPreferenceKey.PRONOUNS, selectedPronoun.name)

			MessageBuilder().append("${getReplyMention(message)} Got it! I'll refer to you with ${selectedPronoun.toReadableList()} from now on.").send(message.channel).get()

		}

	}

}
