package net.krazyweb.hungerbot.commands.game

import net.krazyweb.hungerbot.HungerGamesBot
import net.krazyweb.hungerbot.Permissions
import net.krazyweb.hungerbot.commands.Command
import net.krazyweb.hungergames.game.BotGameEventSubscriber
import net.krazyweb.hungergames.game.TributeStatisticsSubscriber
import net.krazyweb.hungergames.services.*
import net.krazyweb.util.BotProperties
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.MessageBuilder

class LoadGameCommand(private val bot: HungerGamesBot, private val properties: BotProperties) : Command("load", description = "Loads a game from an attached .zip file.", permissions = Permissions.role(properties.getLongProperty("gameHostRoleId"))) {

	override fun handleMessage(message: Message, messageContent: String) {

		if (message.attachments.isEmpty()) {
			MessageBuilder().setContent("No attached games were found. Please attach a .zip file containing a saved game to this command.").send(message.channel)
			return
		}

		val saveService = GamePersistenceService(FileService(properties), EventLoaderService(FileService(properties), PronounService(properties), PlayerPreferenceService(properties)), ItemLoaderService(FileService(properties)))

		val attachment = message.attachments.first()
		bot.activeGame = saveService.load(attachment.downloadAsByteArray().get())

		bot.tributeStatisticsSubscriber = TributeStatisticsSubscriber()
		bot.botGameEventSubscriber = BotGameEventSubscriber(bot, message.channel, properties)

		bot.activeGame!!.addSubscriber(bot.tributeStatisticsSubscriber!!)
		bot.activeGame!!.addSubscriber(bot.botGameEventSubscriber!!)


		MessageBuilder().setContent("Loaded the game!").send(message.channel)

	}

}
