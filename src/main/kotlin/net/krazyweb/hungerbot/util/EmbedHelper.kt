package net.krazyweb.hungerbot.util

import net.krazyweb.hungerbot.HungerGamesBot
import net.krazyweb.hungergames.game.StatisticsKey
import org.javacord.api.entity.channel.TextChannel
import org.javacord.api.entity.message.MessageBuilder
import org.javacord.api.entity.message.embed.EmbedBuilder
import kotlin.math.min

class EmbedHelper {

	companion object {

		fun sendLeaderboardMessages(bot: HungerGamesBot, channel: TextChannel) {

			val leaderboard = bot.tributeStatisticsSubscriber!!.getLeaderboard()

			var output = ""

			leaderboard.forEachIndexed { index, tribute ->
				output += "${index + 1} — ${tribute.name}\n"
			}

			var embed = EmbedBuilder()
					.setTitle("Placements")
					.setDescription(output)

			MessageBuilder().setEmbed(embed).send(channel)

			output = ""

			val districts = bot.activeGame!!.getDistricts().sortedBy { min(leaderboard.indexOf(it.first), leaderboard.indexOf(it.second)) }
			districts.forEachIndexed { index, district ->
				output += "${index + 1} — District ${district.number}\n"
			}

			embed = EmbedBuilder()
					.setTitle("Districts")
					.setDescription(output)

			MessageBuilder().setEmbed(embed).send(channel)

			output = ""

			leaderboard.map { tribute -> tribute to bot.tributeStatisticsSubscriber!!.getStatistics(tribute.discordId).get(StatisticsKey.KILLS, 0) as Int }.sortedByDescending { it.second }.forEach { entry ->
				if (entry.second > 0) {
					output += "${entry.second} — ${entry.first.name}\n"
				}
			}

			embed = EmbedBuilder()
					.setTitle("Kills")
					.setDescription(output)

			MessageBuilder().setEmbed(embed).send(channel)

		}

	}

}
