package net.krazyweb.hungergames.data

import com.fasterxml.jackson.annotation.JsonIgnore
import java.awt.image.BufferedImage

val defaultImage = BufferedImage(128, 128, BufferedImage.TYPE_INT_ARGB)

data class Tribute(val name: String, val discordId: Long, val discordUser: Boolean = true, @JsonIgnore var image: BufferedImage = defaultImage, var alive: Boolean = true, val inventory: MutableMap<Item, Int> = mutableMapOf()) {

	override fun toString(): String {
		return "Tribute(name='$name', discordId=$discordId, isDiscordUser=$discordUser, alive=$alive, inventory=$inventory)"
	}

	fun addItem(item: Item, quantity: Int) {
		if (inventory.containsKey(item)) {
			inventory[item] = inventory[item]!! + quantity
		} else {
			inventory[item] = quantity
		}
	}

	fun removeItem(item: Item, quantity: Int) {
		if (inventory.containsKey(item)) {
			inventory[item] = inventory[item]!! - quantity
			if (inventory[item] == 0) {
				inventory.remove(item)
			}
		}
	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false

		other as Tribute

		if (name != other.name) return false
		if (discordId != other.discordId) return false
		if (discordUser != other.discordUser) return false
		if (alive != other.alive) return false
		if (inventory != other.inventory) return false

		return true
	}

	override fun hashCode(): Int {
		var result = name.hashCode()
		result = 31 * result + discordId.hashCode()
		result = 31 * result + discordUser.hashCode()
		result = 31 * result + alive.hashCode()
		result = 31 * result + inventory.hashCode()
		return result
	}

}


