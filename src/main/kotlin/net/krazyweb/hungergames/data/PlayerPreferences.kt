package net.krazyweb.hungergames.data

enum class PlayerPreferenceKey {
	PRONOUNS, PING_ON_START, STRATEGY
}

data class PlayerPreferences(val user: Long, val preferences: MutableMap<PlayerPreferenceKey, String> = mutableMapOf())
