package net.krazyweb.hungergames.data

data class Item(
		val name: String,
		val plural: String,
		val article: String,
		val tags: List<String> = mutableListOf(),
		val anyTagAllowed: Boolean = true
) {

	fun clone(): Item {
		return Item(name, plural, article, tags, anyTagAllowed)
	}

}
