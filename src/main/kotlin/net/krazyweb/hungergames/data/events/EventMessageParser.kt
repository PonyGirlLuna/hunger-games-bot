package net.krazyweb.hungergames.data.events

import net.krazyweb.hungergames.data.PronounType
import net.krazyweb.hungergames.services.PlayerPreferenceService
import net.krazyweb.hungergames.services.PronounService

class EventMessageParser(val pronounService: PronounService, val playerPreferenceService: PlayerPreferenceService) {

	abstract class TokenHandler {
		abstract fun isStartingToken(character: Char): Boolean
		abstract fun isEndingToken(character: Char): Boolean
		/** If true, this includes the starting token in the data passed to the parseData() function. */
		abstract val includeStartingToken: Boolean
		/** If true, this ignores the ending token and does not attempt to pass it to the next handler. */
		abstract val removeEndingToken: Boolean
		abstract fun parseData(data: String): EventConstructor
	}

	class TextTokenHandler : TokenHandler() {

		//These are not used for basic text handling
		override fun isStartingToken(character: Char): Boolean {
			return false
		}

		override fun isEndingToken(character: Char): Boolean {
			return false
		}

		override val includeStartingToken = false
		override val removeEndingToken = false

		override fun parseData(data: String): EventConstructor {
			return TextEventConstructor(data)
		}

	}

	private val playerNameTokenHandler = object : TokenHandler() {

		override fun isStartingToken(character: Char): Boolean {
			return character.isDigit()
		}

		override fun isEndingToken(character: Char): Boolean {
			return character.isWhitespace() || character in listOf('.', ',', '!', '?', '\'', '(', 's') //TODO Test "s"
		}

		override val includeStartingToken = true
		override val removeEndingToken = false

		override fun parseData(data: String): EventConstructor {
			return PlayerNameEventConstructor(data.toInt() - 1)
		}

	}

	private val pronounTokenHandler = object : TokenHandler() {

		override fun isStartingToken(character: Char): Boolean {
			return character == '('
		}

		override fun isEndingToken(character: Char): Boolean {
			return character == ')'
		}

		override val includeStartingToken = false
		override val removeEndingToken = true

		override fun parseData(data: String): EventConstructor {

			if (data.count { it == '/' } == 1) {

				val (type, player) = data.split("/")
				return PronounEventConstructor(
						when (type) {
							"they" -> PronounType.NOMINATIVE
							"them" -> PronounType.OBJECTIVE
							"their" -> PronounType.POSS_ADJECTIVE
							"theirs" -> PronounType.POSS_PRONOUN
							"themselves" -> PronounType.REFLEXIVE
							else -> PronounType.NOMINATIVE
						},
						player.toInt() - 1,
						"",
						"",
						type.first().isUpperCase(),
						pronounService,
						playerPreferenceService
				)

			} else {

				//TODO Better error message (java.lang.IndexOutOfBoundsException: Index: 1, Size: 1)
				val (type, player, followingTextSingular, followingTextPlural) = data.split("/")
				return PronounEventConstructor(
						when (type) {
							"they" -> PronounType.NOMINATIVE
							"them" -> PronounType.OBJECTIVE
							"their" -> PronounType.POSS_ADJECTIVE
							"theirs" -> PronounType.POSS_PRONOUN
							"themselves" -> PronounType.REFLEXIVE
							else -> PronounType.NOMINATIVE
						},
						player.toInt() - 1,
						followingTextSingular,
						followingTextPlural,
						type.first().isUpperCase(),
						pronounService,
						playerPreferenceService
				)

			}

		}

	}

	private val itemTokenHandler = object : TokenHandler() {

		override fun isStartingToken(character: Char): Boolean {
			return character == '$'
		}

		override fun isEndingToken(character: Char): Boolean {
			return character.isWhitespace() || character in listOf('.', ',', '!', '?', '\'', '(')
		}

		override val includeStartingToken = false
		override val removeEndingToken = false

		override fun parseData(data: String): EventConstructor {
			val pluralType = when (data.substring(0..0)) {
				"p" -> ItemPluralType.PLURAL
				"n" -> ItemPluralType.SINGULAR_WITHOUT_ARTICLE
				else -> ItemPluralType.SINGULAR
			}
			val id = data.substring(1).toInt() - 1
			return ItemNameEventConstructor(id, pluralType)
		}

	}

	private val itemQuantityHandler = object : TokenHandler() {

		override fun isStartingToken(character: Char): Boolean {
			return character == '#'
		}

		override fun isEndingToken(character: Char): Boolean {
			return character.isWhitespace() || character in listOf('.', ',', '!', '?', '\'', '(')
		}

		override val includeStartingToken = false
		override val removeEndingToken = false

		override fun parseData(data: String): EventConstructor {
			val id = data.toInt() - 1
			return ItemQuantityEventConstructor(id)
		}

	}

	private val textHandler = TextTokenHandler()

	private val handlers = listOf(
			playerNameTokenHandler,
			pronounTokenHandler,
			itemTokenHandler,
			itemQuantityHandler
	)

	fun parse(message: String): List<EventConstructor> {

		val output: MutableList<EventConstructor> = mutableListOf()

		var currentTokenHandler: TokenHandler = textHandler //Always start with TextTokenHandler; this is the default state
		var currentData = ""

		for (c in message.toCharArray()) {

			if (currentTokenHandler is TextTokenHandler) {

				val newHandler = handlers.firstOrNull { it.isStartingToken(c) }

				if (newHandler == null) {
					currentData += c
				} else {

					if (currentData.isNotEmpty()) {
						output += currentTokenHandler.parseData(currentData)
					}

					currentTokenHandler = newHandler
					currentData = ""

					if (currentTokenHandler.includeStartingToken) {
						currentData += c
					}

				}

			} else {

				if (currentTokenHandler.isEndingToken(c)) {

					output += currentTokenHandler.parseData(currentData)
					currentData = ""

					if (!currentTokenHandler.removeEndingToken) {
						currentData += c
					}

					val newHandler = handlers.firstOrNull { it.isStartingToken(c) }

					if (newHandler != null) {
						if (!newHandler.includeStartingToken) {
							currentData = ""
						}
						currentTokenHandler = newHandler
					} else {
						currentTokenHandler = textHandler
					}

				} else {
					currentData += c
				}

			}

		}

		if (currentData.isNotEmpty()) {
			output += currentTokenHandler.parseData(currentData)
		}

		return output

	}

}
