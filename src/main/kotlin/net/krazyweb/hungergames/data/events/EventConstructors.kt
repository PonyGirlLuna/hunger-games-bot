package net.krazyweb.hungergames.data.events

import net.krazyweb.hungergames.data.*
import net.krazyweb.hungergames.services.PlayerPreferenceService
import net.krazyweb.hungergames.services.PronounService

abstract class EventConstructor {
	abstract fun generate(event: Event, players: List<Tribute>, itemReferences: List<ItemReference>, items: List<Item>): String
}

data class TextEventConstructor(private val text: String) : EventConstructor() {
	override fun generate(event: Event, players: List<Tribute>, itemReferences: List<ItemReference>, items: List<Item>): String {
		return text
	}
}

data class PlayerNameEventConstructor(val id: Int) : EventConstructor() {
	override fun generate(event: Event, players: List<Tribute>, itemReferences: List<ItemReference>, items: List<Item>): String {
		return "**${players[id].name}**"
	}
}

data class PronounEventConstructor(private val pronounType: PronounType,
                              private val id: Int,
                              private val followingTextPlural: String = "",
                              private val followingTextSingular: String = "",
                              private val capitalized: Boolean = false,
                              private val pronounService: PronounService,
                              private val playerPreferenceService: PlayerPreferenceService) : EventConstructor() {

	override fun generate(event: Event, players: List<Tribute>, itemReferences: List<ItemReference>, items: List<Item>): String {

		val player = players[id]
		val pronouns = pronounService.getPronouns(playerPreferenceService.get(player.discordId, PlayerPreferenceKey.PRONOUNS))

		var output = when (pronounType) {
			PronounType.NOMINATIVE -> pronouns.nominative
			PronounType.OBJECTIVE -> pronouns.objective
			PronounType.POSS_ADJECTIVE -> pronouns.possessiveAdjective
			PronounType.POSS_PRONOUN -> pronouns.possessivePronoun
			PronounType.REFLEXIVE -> pronouns.reflexive
		}

		if (capitalized) {
			output = output.capitalize()
		}

		if (followingTextPlural.isNotBlank()) {
			output += when (pronouns.conjugation) {
				Conjugation.SINGULAR -> " $followingTextSingular"
				Conjugation.PLURAL -> " $followingTextPlural"
			}
		}

		return output

	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false

		other as PronounEventConstructor

		if (pronounType != other.pronounType) return false
		if (id != other.id) return false
		if (followingTextPlural != other.followingTextPlural) return false
		if (followingTextSingular != other.followingTextSingular) return false
		if (capitalized != other.capitalized) return false

		return true
	}

	override fun hashCode(): Int {
		var result = pronounType.hashCode()
		result = 31 * result + id
		result = 31 * result + followingTextPlural.hashCode()
		result = 31 * result + followingTextSingular.hashCode()
		result = 31 * result + capitalized.hashCode()
		return result
	}

}

enum class ItemPluralType {
	SINGULAR, PLURAL, SINGULAR_WITHOUT_ARTICLE
}

data class ItemNameEventConstructor(val itemId: Int, val pluralType: ItemPluralType) : EventConstructor() {
	override fun generate(event: Event, players: List<Tribute>, itemReferences: List<ItemReference>, items: List<Item>): String {
		return when (pluralType) {
			ItemPluralType.SINGULAR -> "${items[itemId].article} **${items[itemId].name}**"
			ItemPluralType.PLURAL -> "**${items[itemId].plural}**"
			ItemPluralType.SINGULAR_WITHOUT_ARTICLE -> "**${items[itemId].name}**"
		}
	}
}


data class ItemQuantityEventConstructor(val itemId: Int) : EventConstructor() {
	override fun generate(event: Event, players: List<Tribute>, itemReferences: List<ItemReference>, items: List<Item>): String {
		return itemReferences[itemId].quantity.toString()
	}
}
