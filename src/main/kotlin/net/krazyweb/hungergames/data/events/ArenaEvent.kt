package net.krazyweb.hungergames.data.events

data class ArenaEvent(val name: String, val timeOfDay: String)
