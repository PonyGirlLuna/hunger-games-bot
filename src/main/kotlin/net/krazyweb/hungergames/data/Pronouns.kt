package net.krazyweb.hungergames.data

enum class PronounType {
	NOMINATIVE, OBJECTIVE, POSS_ADJECTIVE, POSS_PRONOUN, REFLEXIVE
}

enum class Conjugation {
	SINGULAR, PLURAL
}

data class Pronouns(val name: String, val nominative: String, val objective: String, val possessiveAdjective: String, val possessivePronoun: String, val reflexive: String, val conjugation: Conjugation, val selectable: Boolean = true) {
	fun toReadableList(): String {
		return "($name) $nominative/$objective/$possessiveAdjective/$possessivePronoun/$reflexive"
	}
}
