package net.krazyweb.hungergames.data.messages

interface Subscriber {
	fun call(message: Message)
}
