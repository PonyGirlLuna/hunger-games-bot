package net.krazyweb.hungergames.data.messages

import net.krazyweb.hungergames.data.DeathType
import net.krazyweb.hungergames.data.District
import net.krazyweb.hungergames.data.Tribute
import net.krazyweb.hungergames.data.events.Event

abstract class Message

data class EventMessage(val event: Event): Message()
data class DistrictsDecidedMessage(val districts: List<District>) : Message()
data class NonFatalEventMessage(val text: String, val tributes: List<Tribute>) : Message()
data class FatalEventMessage(val text: String, val tributes: List<Tribute>, val killers: List<Tribute>, val defeatedTributes: List<Tribute>, val deathType: DeathType?) : Message()
data class FallenTributesMessage(val defeatedTributes: List<Tribute>) : Message()
data class TimeChangeMessage(val text: String, val day: Int) : Message()
data class ArenaEventMessage(val text: String) : Message()
data class GameOverMessage(val winner: Tribute?) : Message()
