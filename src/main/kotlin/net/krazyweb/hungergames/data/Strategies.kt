package net.krazyweb.hungergames.data

enum class StrategyType {
	PACIFIST, MURDERER, KILL_KILLERS, KILL_INNOCENTS, THREE_QUARTERS, RANDOM
}