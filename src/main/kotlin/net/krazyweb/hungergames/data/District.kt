package net.krazyweb.hungergames.data

data class District(val number: Int, val first: Tribute, val second: Tribute)
