package net.krazyweb.hungergames.tools

import net.krazyweb.hungergames.game.DeadlinessFactor
import kotlin.math.min
import kotlin.math.pow

fun main() {


	val deadlinessFactor = DeadlinessFactor.NORMAL

	for (day in 1..21) {
		val deadliness = min(0.9, getDeadlinessOdds(deadlinessFactor) * (1.0 + getDeadlinessOdds(deadlinessFactor)).pow(day - 1))

		println("$day: $deadliness")
	}

}

private fun getDeadlinessOdds(deadlinessFactor: DeadlinessFactor): Double {
	var deadliness = when (deadlinessFactor) {
		DeadlinessFactor.ALMOST_PACIFIST -> 0.05
		DeadlinessFactor.MILDLY_DEADLY -> 0.1
		DeadlinessFactor.NORMAL -> 0.12
		DeadlinessFactor.QUITE_DANGEROUS -> 0.2
		DeadlinessFactor.EXTRA_SPICY -> 0.3
		DeadlinessFactor.TWI_PLS_NO -> 0.4
	}
	/*if (arenaEvent == "The Bloodbath") {
		deadliness *= 2.0
	}
	if (arenaEvent == "The Feast") {
		deadliness *= 1.5
	}*/
	return deadliness
}
