package net.krazyweb.hungergames.tools

import io.mockk.every
import io.mockk.mockk
import net.krazyweb.hungergames.data.Conjugation
import net.krazyweb.hungergames.data.PlayerPreferenceKey
import net.krazyweb.hungergames.data.Pronouns
import net.krazyweb.hungergames.data.Tribute
import net.krazyweb.hungergames.data.events.Event
import net.krazyweb.hungergames.data.messages.*
import net.krazyweb.hungergames.game.DeadlinessFactor
import net.krazyweb.hungergames.game.HungerGame
import net.krazyweb.hungergames.services.*
import net.krazyweb.util.BotProperties
import java.nio.file.Paths
import kotlin.system.measureTimeMillis


fun main() {

	val numberOfGames = 10000

	val numberOfTributes = 24
	val deadlinessFactor = DeadlinessFactor.NORMAL

	val tributes = (1..numberOfTributes).map { Tribute("$it", it.toLong()) }

	val properties = mockk<BotProperties>()
	every { properties.getPathProperty("workingDirectory") } returns Paths.get("")

	val eventLoaderService = EventLoaderService(FileService(properties), getMockPronounService(), getMockPlayerPreferenceService(tributes))
	val events = eventLoaderService.loadAllEvents()
	val arenaEventList = eventLoaderService.loadAllArenaEvents()
	val items = ItemLoaderService(FileService(properties)).loadAllItems()

	val days = mutableListOf<Double>()
	val deathsPerDay = mutableMapOf<Int, MutableList<Int>>()
	val remainingTributesByDay = mutableMapOf<Int, MutableList<Int>>()
	val arenaEventsPerGame = mutableListOf<Int>()

	val eventsPicked = mutableMapOf<Event, Int>() //event to timesPicked, excludes arena events
	val arenaEventsPicked = mutableMapOf<String, MutableMap<Event, Int>>()
	val arenaEventCount = mutableMapOf<String, Int>()

	val totalTime = measureTimeMillis {
		for (gameRun in 1..numberOfGames) {

			val hungerGame = HungerGame(tributes, deadlinessFactor = deadlinessFactor)
			hungerGame.setEvents(events)
			hungerGame.setArenaEvents(arenaEventList)
			hungerGame.setItems(items)

			var arenaEvents = 0

			hungerGame.addSubscriber(object : Subscriber {
				override fun call(message: Message) {

					if (message is EventMessage) {

						val event = message.event

						if (event.event == null) {
							if (!eventsPicked.containsKey(event)) {
								eventsPicked[event] = 0
							}
							eventsPicked[event] = eventsPicked[event]!! + 1
						} else {
							if (!arenaEventsPicked.containsKey(event.event)) {
								arenaEventsPicked[event.event] = mutableMapOf()
							}
							if (!arenaEventsPicked[event.event]!!.containsKey(event)) {
								arenaEventsPicked[event.event]!![event] = 0
							}
							arenaEventsPicked[event.event]!![event] = arenaEventsPicked[event.event]!![event]!! + 1
						}

					}

					if (message is ArenaEventMessage) {
						if (!arenaEventCount.containsKey(message.text)) {
							arenaEventCount[message.text] = 0
						}
						arenaEventCount[message.text] = arenaEventCount[message.text]!! + 1
					}

					if (message is FallenTributesMessage) {
						if (!remainingTributesByDay.containsKey(hungerGame.day)) {
							remainingTributesByDay[hungerGame.day] = mutableListOf()
						}
						remainingTributesByDay[hungerGame.day]!! += hungerGame.tributes.count { it.alive }
						if (!deathsPerDay.containsKey(hungerGame.day)) {
							deathsPerDay[hungerGame.day] = mutableListOf()
						}
						deathsPerDay[hungerGame.day]!! += hungerGame.tributesThatDiedToday.size
					}

					if (message is ArenaEventMessage && message.text !in listOf("The Bloodbath", "The Feast")) {
						arenaEvents++
					}

					if (message is GameOverMessage) {
						if (!remainingTributesByDay.containsKey(hungerGame.day)) {
							remainingTributesByDay[hungerGame.day] = mutableListOf()
						}
						remainingTributesByDay[hungerGame.day]!! += hungerGame.tributes.count { it.alive }
						if (!deathsPerDay.containsKey(hungerGame.day)) {
							deathsPerDay[hungerGame.day] = mutableListOf()
						}
						deathsPerDay[hungerGame.day]!! += hungerGame.tributesThatDiedToday.size
					}

				}
			})

			hungerGame.start()

			while (!hungerGame.isFinished()) {
				hungerGame.next()
			}

			days += if (hungerGame.time == "night") {
				hungerGame.day + 0.5
			} else {
				hungerGame.day.toDouble()
			}

			arenaEventsPerGame += arenaEvents

			if (gameRun % 100 == 0) {
				println("Finished $gameRun")
			}

		}
	}

	println("Completed in ${(totalTime / 1000.0).format(1)} seconds.\n\n")

	println("Average game length: ${days.average()} days.")
	println("Average number of arena events: ${arenaEventsPerGame.average()}\n")

	println("Average deaths per day:")
	deathsPerDay.entries.forEach {
		println("${it.key} - ${it.value.average().format(2)} (${it.value.count { it != 0 }}/${it.value.size})")
	}

	println("\nAverage alive tributes by the end of each day:")
	remainingTributesByDay.entries.forEach {
		println("${it.key} - ${it.value.average().format(2)}")
	}

	println("\nEvents picked (average times per game):")
	eventsPicked.entries.filter { it.key.killed.isEmpty() }.sortedByDescending { it.value }.map { it.key to it.value / numberOfGames.toDouble() }.forEach {
		println("${it.second.format(3)}\t\t1 / ${(1.0 / it.second).format(1)}\t\t${it.first.isItemEvent()} ${it.first.isItemUseEvent()} ${it.first.isItemGainEvent()}\t\t${it.first.messageTemplate}")
	}

	println("\n\n")
	eventsPicked.entries.filter { it.key.killed.isNotEmpty() }.sortedByDescending { it.value }.map { it.key to it.value / numberOfGames.toDouble() }.forEach {
		println("${it.second.format(3)}\t\t1 / ${(1.0 / it.second).format(1)}\t\t${it.first.isItemEvent()} ${it.first.isItemUseEvent()} ${it.first.isItemGainEvent()}\t\t${it.first.messageTemplate}")
	}

	arenaEventsPicked.keys.forEach { arenaEvent ->

		val arenaEventCounts = arenaEventsPicked[arenaEvent]!!
		val numberOfArenaEvents = arenaEventCount[arenaEvent]!!

		println("\nArena Events picked $arenaEvent (average times per event):")
		arenaEventCounts.entries.filter { it.key.killed.isEmpty() }.sortedByDescending { it.value }.map { it.key to it.value / numberOfArenaEvents.toDouble() }.forEach {
			println("${it.second.format(3)}\t\t1 / ${(1.0 / it.second).format(1)}\t\t${it.first.isItemEvent()} ${it.first.isItemUseEvent()} ${it.first.isItemGainEvent()}\t\t${it.first.messageTemplate}")
		}

		println("")
		arenaEventCounts.entries.filter { it.key.killed.isNotEmpty() }.sortedByDescending { it.value }.map { it.key to it.value / numberOfArenaEvents.toDouble() }.forEach {
			println("${it.second.format(3)}\t\t1 / ${(1.0 / it.second).format(1)}\t\t${it.first.isItemEvent()} ${it.first.isItemUseEvent()} ${it.first.isItemGainEvent()}\t\t${it.first.messageTemplate}")
		}

	}

	val totalEventCount = eventsPicked.values.sum().toDouble()
	println("\n\nNon-item events: ${eventsPicked.entries.filter { !it.key.isItemEvent() }.sumByDouble { it.value.toDouble() } / totalEventCount}")
	println("Item gain events: ${eventsPicked.entries.filter { it.key.isItemGainEvent() }.sumByDouble { it.value.toDouble() } / totalEventCount}")
	println("Item use events: ${eventsPicked.entries.filter { it.key.isItemUseEvent() }.sumByDouble { it.value.toDouble() } / totalEventCount}")

}

fun getMockPlayerPreferenceService(tributes: List<Tribute>): PlayerPreferenceService {

	val playerPreferenceService = mockk<PlayerPreferenceService>()

	tributes.forEach {
		every { playerPreferenceService.get(it.discordId, PlayerPreferenceKey.PRONOUNS) } returns "they"
	}

	return playerPreferenceService

}

fun getMockPronounService(): PronounService {

	val pronounService = mockk<PronounService>()
	every { pronounService.getPronouns("he") } returns Pronouns("he", "he", "him", "his", "his", "himself", Conjugation.SINGULAR)
	every { pronounService.getPronouns("she") } returns Pronouns("she", "she", "her", "her", "hers", "herself", Conjugation.SINGULAR)
	every { pronounService.getPronouns("they") } returns Pronouns("they", "they", "them", "their", "theirs", "themselves", Conjugation.PLURAL)

	return pronounService

}
