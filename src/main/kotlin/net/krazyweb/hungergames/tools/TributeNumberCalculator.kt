package net.krazyweb.hungergames.tools

import kotlin.math.min
import kotlin.math.pow

fun main() {

	val totalTributes = 24

	for (i in totalTributes.downTo(2)) {

		val maxAvailableTributes = min(5, i)

		println("$i: " + getTributesWeighting(i, totalTributes, maxAvailableTributes))

	}

}

private fun getTributesWeighting(tributesAlive: Int, totalTributes: Int, maxAvailableTributes: Int): Map<Int, String> {

	val maxTributesWeighting = getMaxTributesWeighting(maxAvailableTributes)
	val minTributesWeighting = getMinTributesWeighting(maxAvailableTributes)

	val interpolation = ((1.0 - (tributesAlive / totalTributes.toDouble())) * 1.05).pow(3.0)

	val finalWeights = mutableMapOf<Int, Double>()

	for (i in 1..maxAvailableTributes) {
		finalWeights[i] = maxTributesWeighting[i]!! + interpolation * (minTributesWeighting[i]!! - maxTributesWeighting[i]!!)
	}

	val total = finalWeights.values.sum()

	finalWeights.forEach {
		finalWeights[it.key] = (it.value / total)
	}

	val sorted = finalWeights.toSortedMap()
	sorted.forEach {
		finalWeights[it.key] = sorted.values.take(it.key).sum()
		if (1.0 - finalWeights[it.key]!! < 0.00001) {
			finalWeights[it.key] = 1.0
		}
	}

	return sorted.mapValues { (it.value * 100).format(0) + "%" }

}

private fun getMaxTributesWeighting(maxAvailableTributes: Int): MutableMap<Int, Double> {

	val weights = mutableMapOf<Int, Double>()

	for (i in 1..maxAvailableTributes) {
		weights[i] = i.toDouble().pow(-0.75)
	}

	val total = weights.values.sum()

	weights.forEach {
		weights[it.key] = it.value / total
	}

	return weights

}

private fun getMinTributesWeighting(maxAvailableTributes: Int): MutableMap<Int, Double> {

	val weights = mutableMapOf<Int, Double>()

	for (i in 1..maxAvailableTributes) {
		var t = i.toDouble()
		if (i < 2) {
			t += 2.5
		}
		weights[i] = t.pow(-4.0)
	}

	val total = weights.values.sum()

	weights.forEach {
		weights[it.key] = it.value / total
	}

	return weights

}

fun Double.format(digits: Int) = "%.${digits}f".format(this)
