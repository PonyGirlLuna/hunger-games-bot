package net.krazyweb.hungergames.game

import net.krazyweb.hungergames.data.Tribute
import net.krazyweb.hungergames.data.messages.FatalEventMessage
import net.krazyweb.hungergames.data.messages.GameOverMessage
import net.krazyweb.hungergames.data.messages.Message
import net.krazyweb.hungergames.data.messages.Subscriber

class TributeStatisticsSubscriber : Subscriber {

	private val statistics = mutableListOf<TributeStatistics>()
	private val leaderboard = mutableListOf<Tribute>()

	override fun call(message: Message) {
		if (message is FatalEventMessage) {
			message.killers.forEach {
				val stats = getStatistics(it.discordId)
				stats.put(StatisticsKey.KILLS, (stats.get(StatisticsKey.KILLS, 0) as Int) + message.defeatedTributes.filter { defeated -> defeated != it }.size)
			}
			leaderboard += message.defeatedTributes
		} else if (message is GameOverMessage) {
			if (message.winner != null) {
				leaderboard += message.winner
			}
		}
	}

	private fun createStatsIfNotExists(id: Long) {
		if (statistics.none { it.tributeId == id }) {
			statistics += TributeStatistics(id)
		}
	}

	fun getStatistics(id: Long): TributeStatistics {
		createStatsIfNotExists(id)
		return statistics.single { it.tributeId == id }
	}

	/** Returns the placements of each tribute, with first place at index 0 (the winner of the games). This list is only updated when tributes die. */
	fun getLeaderboard(): List<Tribute> {
		return leaderboard.reversed()
	}

}
