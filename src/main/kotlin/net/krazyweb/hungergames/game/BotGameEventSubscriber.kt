package net.krazyweb.hungergames.game

import net.krazyweb.hungerbot.HungerGamesBot
import net.krazyweb.hungerbot.util.EmbedHelper
import net.krazyweb.hungergames.data.messages.*
import net.krazyweb.hungergames.services.*
import net.krazyweb.util.BotProperties
import net.krazyweb.util.ImageUtils
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.javacord.api.entity.channel.TextChannel
import org.javacord.api.entity.message.MessageBuilder
import org.javacord.api.entity.message.embed.EmbedBuilder
import java.awt.Color
import java.nio.file.Files

class BotGameEventSubscriber(val bot: HungerGamesBot, val channel: TextChannel, val botProperties: BotProperties) : Subscriber {

	val log: Logger = LogManager.getLogger(BotGameEventSubscriber::class.java)

	override fun call(message: Message) {

		when (message) {
			is NonFatalEventMessage -> {

				val messageText = message.text

				MessageBuilder().setEmbed(EmbedBuilder()
						.setColor(Color(0.2f, 0.7f, 0.5f, 1.0f)) //TODO Define these colors somewhere and reuse them
						.setDescription(messageText)
						.setImage(ImageUtils.createEventParticipantsImage(message.tributes))
				).send(channel)

			}
			is FatalEventMessage -> {

				val killedTributeCount = message.defeatedTributes.size

				//TODO Vary these messages slightly (echoes throughout the arena, etc)
				val cannonShotMessage = if (killedTributeCount == 1) {
					"**1** cannon shot is heard in the distance."
				} else {
					"**$killedTributeCount** cannon shots are heard in the distance."
				}

				MessageBuilder().setEmbed(EmbedBuilder()
						.setColor(Color(0.8f, 0.2f, 0.1f, 1.0f))
						.setDescription(cannonShotMessage)
				).send(channel)

				//TODO Find a good value for this. Maybe make it configurable?
				Thread.sleep(3000)

				val messageText = ":skull_crossbones: ${message.text}"

				MessageBuilder().setEmbed(EmbedBuilder()
						.setColor(Color(0.8f, 0.2f, 0.1f, 1.0f))
						.setDescription(messageText)
						.setImage(ImageUtils.createEventParticipantsImage(message.tributes))
				).send(channel).get()

				Thread.sleep(100)

				if (botProperties.getBooleanProperty("flutterbotEnabled")) {
					message.defeatedTributes.filter { it.discordUser }.forEach { killedTribute ->
						val killedTributeMessage = MessageBuilder().setContent("flutter kill <@${killedTribute.discordId}>").send(channel).get()
						killedTributeMessage.delete()
					}
				}

			}
			is DistrictsDecidedMessage -> {

				MessageBuilder().setEmbed(EmbedBuilder()
						.setColor(Color(0.3f, 0.6f, 0.9f, 1.0f))
						.setImage(ImageUtils.createScoreboard(bot.activeGame!!.getDistricts(), bot.tributeStatisticsSubscriber!!))
				).send(channel)

			}
			is ArenaEventMessage -> {

				MessageBuilder().setEmbed(EmbedBuilder()
						.setColor(Color(0.8f, 0.2f, 0.1f, 1.0f))
						.setImage(ImageUtils.createArenaEventImage(message.text))
				).send(channel)

			}
			is TimeChangeMessage -> {

				val color = if (message.text.toLowerCase() == "day") {
					Color.decode("#c6af14")
				} else {
					Color.decode("#8f61cf")
				}

				MessageBuilder().setEmbed(EmbedBuilder()
						.setColor(color)
						.setImage(ImageUtils.createTimeChangeImage(message.text, message.day))
				).send(channel)

			}
			is GameOverMessage -> {

				bot.autoGameService.running.set(false)

				//TODO This better
				val outputDirectory = botProperties.getPathProperty("workingDirectory").resolve("saved-games")
				Files.createDirectories(outputDirectory)

				val fileService = FileService(botProperties)
				val pronounService = PronounService(botProperties)
				val playerPreferenceService = PlayerPreferenceService(botProperties)
				val eventLoaderService = EventLoaderService(fileService, pronounService, playerPreferenceService)
				val itemLoaderService = ItemLoaderService(fileService)

				//TODO Is there a way to handle this better?
				Thread.sleep(5000)

				MessageBuilder().setEmbed(EmbedBuilder()
						.setColor(Color(0.2f, 0.7f, 0.5f, 1.0f))
						.setImage(ImageUtils.createWinnerImage(message.winner!!, bot.activeGame!!.getDistricts()))
				).send(channel).get()

				Thread.sleep(5000)

				if (botProperties.getBooleanProperty("flutterbotEnabled")) {
					try {
						val resetMessage = MessageBuilder().setContent("flutter reset").send(channel).get()
						resetMessage.delete()
						if (message.winner.discordUser) {
							val victorMessage = MessageBuilder().setContent("flutter victor <@${message.winner.discordId}>").send(channel).get()
							victorMessage.delete()
						}
					} catch (e: Exception) {
						log.error(e, e)
						MessageBuilder().setContent("There was an error resetting the tributes. <@${botProperties.getLongProperty("yellAtMeWhenErrorsUnmutingId")}>!").send(channel)
					}
				}

				try {
					Files.write(outputDirectory.resolve("${bot.activeGame!!.seed}-finished.zip"), GamePersistenceService(fileService, eventLoaderService, itemLoaderService).save(bot.activeGame!!))
				} catch (e: Exception) {
					log.error("Error while saving the game!", e)
				}

				Thread.sleep(5000)

				EmbedHelper.sendLeaderboardMessages(bot, channel)

			}
			is FallenTributesMessage -> {

				if (message.defeatedTributes.isNotEmpty()) {

					MessageBuilder().setEmbed(EmbedBuilder()
							.setImage(ImageUtils.createFallenTributesImage(message.defeatedTributes, bot.activeGame!!.getDistricts()))
					).send(channel)

				} else {

					MessageBuilder().setEmbed(EmbedBuilder()
							.setColor(Color(0.2f, 0.7f, 0.5f, 1.0f))
							.setTitle("Fallen Tributes")
							.setDescription("No tributes fell today.")
					).send(channel)

				}

			}
			is EventMessage -> {
				//Do nothing! We don't need this one. (But we still need to handle it)
			}
			else -> {
				MessageBuilder().setContent("Unknown message type! $message").send(channel)
			}
		}

	}

}
