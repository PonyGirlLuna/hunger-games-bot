package net.krazyweb.hungergames.game

import net.krazyweb.hungergames.data.events.ArenaEvent
import kotlin.random.Random

interface TimeHandler {
	fun getNextTimeHandler(random: Random, hungerGame: HungerGame): TimeHandler
	fun getName(): String
	fun arenaEvent(): ArenaEvent? {
		return null
	}
	fun shouldIncrementDay(): Boolean {
		return false
	}
	fun playersCanTakeActions(): Boolean {
		return true
	}
}

class BloodbathTimeHandler : TimeHandler {

	override fun getNextTimeHandler(random: Random, hungerGame: HungerGame): TimeHandler {
		return DayTimeHandler()
	}

	override fun arenaEvent(): ArenaEvent {
		return ArenaEvent("The Bloodbath", "day")
	}

	override fun getName(): String {
		return "The Bloodbath"
	}

}

class DayTimeHandler : TimeHandler {

	override fun getNextTimeHandler(random: Random, hungerGame: HungerGame): TimeHandler {
		return if (hungerGame.day == 5) {
			ArenaEventTimeHandler(ArenaEvent("The Feast", "day"))
		} else if (random.nextInt(6) == 0 && hungerGame.day > 1) { //TODO Test that multiple arena events can't happen in one day (but how?)
			val possibleArenaEvents = hungerGame.getAllArenaEvents()
			if (possibleArenaEvents.isEmpty()) {
				NightTimeHandler()
			} else {
				val selectedArenaEvent = possibleArenaEvents.random(random)
				if (selectedArenaEvent.timeOfDay.toLowerCase() == "night") {
					hungerGame.nextArenaEvent = selectedArenaEvent
					NightTimeHandler()
				} else {
					ArenaEventTimeHandler(selectedArenaEvent)
				}
			}
		} else {
			NightTimeHandler()
		}
	}

	override fun getName(): String {
		return "Day"
	}

	override fun shouldIncrementDay(): Boolean {
		return true
	}

}

class NightTimeHandler : TimeHandler {

	override fun getNextTimeHandler(random: Random, hungerGame: HungerGame): TimeHandler {
		return if (hungerGame.nextArenaEvent != null) {
			ArenaEventTimeHandler(hungerGame.nextArenaEvent!!)
		} else {
			FallenTributesTimeHandler()
		}
	}

	override fun getName(): String {
		return "Night"
	}

}

class FallenTributesTimeHandler : TimeHandler {

	override fun getNextTimeHandler(random: Random, hungerGame: HungerGame): TimeHandler {
		return DayTimeHandler()
	}

	override fun getName(): String {
		return "Fallen Tributes"
	}

	override fun playersCanTakeActions(): Boolean {
		return false
	}

}

class ArenaEventTimeHandler(val event: ArenaEvent) : TimeHandler {

	override fun getNextTimeHandler(random: Random, hungerGame: HungerGame): TimeHandler {
		return when (event.timeOfDay.toLowerCase()) {
			"day" -> NightTimeHandler()
			"night" -> FallenTributesTimeHandler()
			else -> throw IllegalArgumentException("Arena event '${event.name}' must have either 'day' or 'night' for time of day.")
		}
	}

	override fun arenaEvent(): ArenaEvent {
		return event
	}

	override fun getName(): String {
		return event.name
	}

}
