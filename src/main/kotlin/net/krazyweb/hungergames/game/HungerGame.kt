package net.krazyweb.hungergames.game

import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.annotation.JsonIgnore
import net.krazyweb.hungerbot.HungerGamesBot
import net.krazyweb.hungergames.data.District
import net.krazyweb.hungergames.data.Item
import net.krazyweb.hungergames.data.Tribute
import net.krazyweb.hungergames.data.events.ArenaEvent
import net.krazyweb.hungergames.data.events.Event
import net.krazyweb.hungergames.data.messages.*
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.math.min
import kotlin.math.pow
import kotlin.random.Random


enum class DeadlinessFactor {
	ALMOST_PACIFIST, MILDLY_DEADLY, NORMAL, QUITE_DANGEROUS, EXTRA_SPICY, TWI_PLS_NO
}

enum class ItemEventType {
	NONE, GAINS_ITEM, USES_ITEM
}

data class ArenaData(val day: Int, val time: String, val activeEvent: ArenaEvent?)

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
class HungerGame(val tributes: List<Tribute>, val seed: Long = System.currentTimeMillis(), private val deadlinessFactor: DeadlinessFactor = DeadlinessFactor.NORMAL) {

	@JsonIgnore
	private val log: Logger = LogManager.getLogger(HungerGame::class.java)

	@JsonIgnore
	private var random = Random(seed)

	@JsonIgnore
	private val messageSubscribers: MutableList<Subscriber> = mutableListOf()

	@JsonIgnore
	private val events: MutableList<Event> = mutableListOf()

	@JsonIgnore
	private val arenaEvents: MutableList<ArenaEvent> = mutableListOf()

	@JsonIgnore
	private val items: MutableList<Item> = mutableListOf()

	@JsonIgnore
	private var timeHandler: TimeHandler = BloodbathTimeHandler()

	@JsonIgnore
	private val processing = AtomicBoolean(false)

	/** Makes sure we have a reference to which version of the sim the game was run on so it can be properly replayed */
	private val commitHash = try {
		Properties().apply { load(HungerGamesBot::class.java.classLoader.getResourceAsStream("build.properties")) }.getProperty("commit")
	} catch (e: NullPointerException) {
		log.error(e, e)
		"build.properties not found!"
	}

	var started = false
	/** Due to the lack of a Random class in which we can track the seed, this will be used to fast-forward games to recover from errors (replay with this seed up to move number X) */
	var moveNumber = 0
	var day = 1
	var time = "day"

	var arenaEvent: ArenaEvent? = null
	var nextArenaEvent: ArenaEvent? = null
	val arenaEventsUsed = mutableListOf<ArenaEvent>()

	val tributesNeedingMovesThisRound: MutableList<Tribute> = mutableListOf()
	val tributesThatDiedToday: MutableList<Tribute> = mutableListOf()

	fun setEvents(events: List<Event>) {
		this.events.clear()
		this.events += events
		log.debug("Set ${this.events.size} events")
	}

	fun setArenaEvents(arenaEvents: List<ArenaEvent>) {
		this.arenaEvents.clear()
		this.arenaEvents += arenaEvents
		log.debug("Set ${this.arenaEvents.size} arenaEvents")
	}

	fun setItems(items: List<Item>) {
		this.items.clear()
		this.items += items
		log.debug("Set ${this.items.size} items")
	}

	fun addSubscriber(subscriber: Subscriber) {
		messageSubscribers += subscriber
		log.debug("Added subscriber: $subscriber")
	}

	fun removeSubscriber(subscriber: Subscriber) {
		messageSubscribers -= subscriber
		log.debug("Removed subscriber: $subscriber")
	}

	private fun sendMessage(message: Message) {
		log.debug("Sending message: $message to each subscriber")
		messageSubscribers.forEach { it.call(message) }
	}

	/** Takes care of all the housekeeping needed to start a new game. Will not produce any actual gameplay steps itself. */
	fun start() {

		if (processing.get()) {
			log.debug("Currently processing.")
			return
		}

		processing.set(true)

		if (events.isEmpty()) {
			processing.set(false)
			throw IllegalStateException("No events loaded for game!")
		}

		if (tributes.isEmpty() || tributes.size % 2 != 0) {
			processing.set(false)
			throw IllegalStateException("Tributes are empty or an odd number was provided")
		}

		log.debug("$commitHash found for source code")

		random = Random(seed)

		tributesNeedingMovesThisRound.clear()
		tributesThatDiedToday.clear()
		day = 1
		time = "day"
		arenaEvent = null
		nextArenaEvent = null
		arenaEventsUsed.clear()
		timeHandler = BloodbathTimeHandler()
		tributes.forEach {
			//Clear all tribute data (items, relationships, etc.). Make sure to add tests when changing this.
			it.alive = true
			it.inventory.clear()
		}
		started = true

		sendMessage(DistrictsDecidedMessage(getDistricts()))

		processing.set(false)

	}

	@JsonIgnore
	fun getDistricts(): List<District> {
		val districtRandom = Random(seed)
		return tributes.shuffled(districtRandom).chunked(2).mapIndexed { index, it -> District(index + 1, it[0], it[1]) }
	}

	@JsonIgnore
	fun getAllArenaEvents(): List<ArenaEvent> {
		return arenaEvents.filter { it !in arenaEventsUsed }
	}

	@JsonIgnore
	fun getMaxNumberOfTributesInEvents(): Int {
		return events.map { it.getTotalNumberOfTributes() }.maxOrNull()!!
	}

	@JsonIgnore
	fun isFinished(): Boolean {
		return tributes.count { it.alive } <= 1
	}

	fun replay() {

		val targetMoveNumber = moveNumber
		moveNumber = 0

		random = Random(seed)

		if (started) {
			start()
		}

		while (moveNumber != targetMoveNumber) {
			next()
		}

	}

	/** Progresses the game to the next step. */
	fun next() {

		if (processing.get()) {
			log.trace("Processing something! Please try again in a moment.")
			log.trace(Thread.currentThread().stackTrace.joinToString("\n") { "${it.fileName}.${it.methodName}:${it.lineNumber}" })
			return
		}

		//The game is over. Do nothing!
		if (tributes.count { it.alive } <= 1) {
			return
		}

		processing.set(true)

		moveNumber++

		if (tributesNeedingMovesThisRound.isNotEmpty()) {

			val arenaData = ArenaData(day, time, arenaEvent)
			var maxAttempts = 100

			var fatal: Boolean
			var selectedTributes: List<Tribute>
			var possibleEvents: List<Event>

			do {

				//Increase danger every day, up to a maximum of a 70% chance of fatal events.
				//This should roughly be a 75% chance of fatal events by day 14 with a deadliness setting of 0.1.
				//TODO Turn this into that more complicated global "pressure" stat
				val deadliness = min(0.9, getDeadlinessOdds() * (1.0 + getDeadlinessOdds()).pow(day - 1))
				log.debug("Deadliness: $deadliness")

				val fatalRoll = random.nextDouble()
				log.debug("Fatal roll: $fatalRoll")

				fatal = fatalRoll < deadliness

				val maxAvailableTributes = min(tributesNeedingMovesThisRound.size, min(tributes.count { tribute -> tribute.alive }, getMaxNumberOfTributesInEvents()))

				val numberOfTributesSelected = getNumberOfTributesForEvent(maxAvailableTributes)
				log.debug("$numberOfTributesSelected tributes selected for an event")

				selectedTributes = tributesNeedingMovesThisRound.shuffled(random).take(numberOfTributesSelected)

				val fatalMultiplier = if (fatal) { 1.3 } else { 1.0 }

				val usesItemChance = min(0.65, selectedTributes.sumByDouble { it.inventory.keys.size.toDouble() } / (3.0 * selectedTributes.size))

				val itemEventType = if (selectedTributes.any { it.inventory.isNotEmpty() }) {
					if (random.nextDouble() < 0.5 * fatalMultiplier) {
						if (random.nextDouble() < usesItemChance * fatalMultiplier) {
							ItemEventType.USES_ITEM
						} else {
							ItemEventType.GAINS_ITEM
						}
					} else {
						ItemEventType.NONE
					}
				} else {
					if (random.nextDouble() < 0.5) {
						ItemEventType.GAINS_ITEM
					} else {
						ItemEventType.NONE
					}
				}

				possibleEvents = events.filter { it.canRunEvent(selectedTributes, fatal, itemEventType, arenaData) && tributes.count { tribute -> tribute.alive } - it.killed.size > 0 }

			} while (possibleEvents.isEmpty() && maxAttempts-- > 0)

			if (possibleEvents.isEmpty()) {
				processing.set(false)
				throw RuntimeException("Somehow could not find a valid event!")
			}

			val chosenEvent = possibleEvents.random(random)
			tributesNeedingMovesThisRound.removeAll(selectedTributes)

			sendMessage(EventMessage(chosenEvent))

			if (fatal) {

				val killedTributes = chosenEvent.getKilledTributes(selectedTributes)
				val killerTributes = chosenEvent.getKillerTributes(selectedTributes)

				tributesThatDiedToday += killedTributes

				sendMessage(FatalEventMessage(chosenEvent.runEvent(selectedTributes, items, random), selectedTributes, killerTributes, killedTributes, chosenEvent.deathType))

			} else {

				sendMessage(NonFatalEventMessage(chosenEvent.runEvent(selectedTributes, items, random), selectedTributes))

			}

			if (tributes.count { it.alive } <= 1) {
				sendMessage(GameOverMessage(tributes.firstOrNull { it.alive }))
			}

		} else {

			if (tributes.count { it.alive } > 1) {

				if (timeHandler.shouldIncrementDay() && arenaEvent?.name != "The Bloodbath") {
					day++
				}

			}

			arenaEvent = null

			if (tributes.count { it.alive } > 1) {

				if (timeHandler.playersCanTakeActions()) {
					tributesNeedingMovesThisRound += tributes.filter { it.alive }
				}

				when {
					timeHandler.arenaEvent() != null -> {
						arenaEvent = timeHandler.arenaEvent()
						arenaEventsUsed += arenaEvent!!
						nextArenaEvent = null
						sendMessage(ArenaEventMessage(arenaEvent!!.name))
					}
					timeHandler is FallenTributesTimeHandler -> {
						sendMessage(FallenTributesMessage(tributesThatDiedToday))
						tributesThatDiedToday.clear()
					}
					else -> {
						time = timeHandler.getName()
						sendMessage(TimeChangeMessage(timeHandler.getName(), day))
					}
				}

				timeHandler = timeHandler.getNextTimeHandler(random, this)

			}

		}

		processing.set(false)

	}

	private fun getDeadlinessOdds(): Double {
		var deadliness = when (deadlinessFactor) {
			DeadlinessFactor.ALMOST_PACIFIST -> 0.05
			DeadlinessFactor.MILDLY_DEADLY -> 0.1
			DeadlinessFactor.NORMAL -> 0.12
			DeadlinessFactor.QUITE_DANGEROUS -> 0.2
			DeadlinessFactor.EXTRA_SPICY -> 0.3
			DeadlinessFactor.TWI_PLS_NO -> 0.4
		}
		if (arenaEvent?.name == "The Bloodbath") {
			deadliness *= 2.2
		}
		if (arenaEvent?.name == "The Feast") {
			deadliness *= 1.5
		}
		if (tributes.count { it.alive } == 4) {
			deadliness *= 1.15
		}
		if (tributes.count { it.alive } == 3) {
			deadliness *= 1.5
		}
		if (tributes.count { it.alive } == 2) {
			deadliness *= 2.0
		}
		return deadliness
	}

	private fun getNumberOfTributesForEvent(maxAvailableTributes: Int): Int {
		return getTributesWeighting(maxAvailableTributes).entries.first { it.value >= random.nextDouble() }.key
	}

	private fun getTributesWeighting(maxAvailableTributes: Int): MutableMap<Int, Double> {

		val tributesAlive = tributes.count { tribute -> tribute.alive }
		val totalTributes = tributes.size

		val maxTributesWeighting = getMaxTributesWeighting(maxAvailableTributes)
		val minTributesWeighting = getMinTributesWeighting(maxAvailableTributes)

		val interpolation = ((1.0 - (tributesAlive / totalTributes.toDouble())) * 1.05).pow(3.0)

		val finalWeights = mutableMapOf<Int, Double>()

		for (i in 1..maxAvailableTributes) {
			finalWeights[i] = maxTributesWeighting[i]!! + interpolation * (minTributesWeighting[i]!! - maxTributesWeighting[i]!!)
		}

		val total = finalWeights.values.sum()

		finalWeights.forEach {
			finalWeights[it.key] = (it.value / total)
		}

		log.debug("Tribute number weights: $finalWeights")

		val sorted = finalWeights.toSortedMap()
		sorted.forEach {
			finalWeights[it.key] = sorted.values.take(it.key).sum()
			if (1.0 - finalWeights[it.key]!! < 0.00001) {
				finalWeights[it.key] = 1.0
			}
		}

		return finalWeights

	}

	private fun getMaxTributesWeighting(maxAvailableTributes: Int): MutableMap<Int, Double> {

		val weights = mutableMapOf<Int, Double>()

		for (i in 1..maxAvailableTributes) {
			weights[i] = i.toDouble().pow(-0.75)
		}

		val total = weights.values.sum()

		weights.forEach {
			weights[it.key] = it.value / total
		}

		return weights

	}

	private fun getMinTributesWeighting(maxAvailableTributes: Int): MutableMap<Int, Double> {

		val weights = mutableMapOf<Int, Double>()

		for (i in 1..maxAvailableTributes) {
			var t = i.toDouble()
			if (i < 2) {
				t += 2.5
			}
			weights[i] = t.pow(-4.0)
		}

		val total = weights.values.sum()

		weights.forEach {
			weights[it.key] = it.value / total
		}

		return weights

	}

	override fun toString(): String {
		return "HungerGame(tributes=$tributes, seed=$seed, deadlinessFactor=$deadlinessFactor, messageSubscribers=$messageSubscribers, events=$events, arenaEvents=$arenaEvents, arenaEventsUsed=$arenaEventsUsed, nextArenaEvent=$nextArenaEvent, timeHandler=$timeHandler, commitHash='$commitHash', moveNumber=$moveNumber, day=$day, time='$time', arenaEvent=$arenaEvent, tributesNeedingMovesThisRound=$tributesNeedingMovesThisRound, tributesThatDiedToday=$tributesThatDiedToday)"
	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false

		other as HungerGame

		if (started != other.started) return false
		if (tributes != other.tributes) return false
		if (seed != other.seed) return false
		if (deadlinessFactor != other.deadlinessFactor) return false
		if (events != other.events) return false
		if (arenaEvents != other.arenaEvents) return false
		if (nextArenaEvent != other.nextArenaEvent) return false
		if (items != other.items) return false
		if (timeHandler.javaClass != other.timeHandler.javaClass) return false
		if (commitHash != other.commitHash) return false
		if (moveNumber != other.moveNumber) return false
		if (day != other.day) return false
		if (time != other.time) return false
		if (arenaEvent != other.arenaEvent) return false
		if (arenaEventsUsed != other.arenaEventsUsed) return false
		if (tributesNeedingMovesThisRound != other.tributesNeedingMovesThisRound) return false
		if (tributesThatDiedToday != other.tributesThatDiedToday) return false

		return true
	}

	override fun hashCode(): Int {
		var result = tributes.hashCode()
		result = 31 * result + started.hashCode()
		result = 31 * result + seed.hashCode()
		result = 31 * result + deadlinessFactor.hashCode()
		result = 31 * result + events.hashCode()
		result = 31 * result + arenaEvents.hashCode()
		result = 31 * result + (nextArenaEvent?.hashCode() ?: 0)
		result = 31 * result + items.hashCode()
		result = 31 * result + timeHandler.javaClass.hashCode()
		result = 31 * result + (commitHash?.hashCode() ?: 0)
		result = 31 * result + moveNumber
		result = 31 * result + day
		result = 31 * result + time.hashCode()
		result = 31 * result + (arenaEvent?.hashCode() ?: 0)
		result = 31 * result + arenaEventsUsed.hashCode()
		result = 31 * result + tributesNeedingMovesThisRound.hashCode()
		result = 31 * result + tributesThatDiedToday.hashCode()
		return result
	}

}
