package net.krazyweb.hungergames.services

import net.krazyweb.hungergames.data.StrategyType
import net.krazyweb.util.BotProperties

class StrategyService(val properties: BotProperties) {

	fun getStrategyNameList(): List<String> {
		return listOf("pacifist", "murderer", "avenger", "opportunist", "strategist")
	}
	
	fun getStrategyName (strategy: StrategyType): String {
		return when (strategy) {
			StrategyType.PACIFIST -> "pacifist"
			StrategyType.MURDERER -> "murderer" 
			StrategyType.KILL_KILLERS -> "avenger"
			StrategyType.KILL_INNOCENTS -> "opportunist"
			StrategyType.THREE_QUARTERS -> "strategist"
			else -> "random"
		}	
	}
	
	fun getStrategy (strategyName: String): StrategyType {
		return when(strategyName) {
			"pacifist" -> StrategyType.PACIFIST
			"murderer" -> StrategyType.MURDERER
			"avenger" -> StrategyType.KILL_KILLERS
			"opportunist" -> StrategyType.KILL_INNOCENTS
			"strategist" -> StrategyType.THREE_QUARTERS
			else -> StrategyType.RANDOM
		}
	}

}
