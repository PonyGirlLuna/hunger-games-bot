package net.krazyweb.hungergames.services

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import net.krazyweb.hungergames.data.Item
import net.krazyweb.hungergames.data.events.ArenaEvent
import net.krazyweb.hungergames.data.events.Event
import net.krazyweb.hungergames.game.HungerGame
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import java.util.zip.ZipOutputStream
import javax.imageio.ImageIO

class GamePersistenceService(private val fileService: FileService, private val eventLoaderService: EventLoaderService, private val itemLoaderService: ItemLoaderService) {

	private val log: Logger = LogManager.getLogger(GamePersistenceService::class.java)

	/**
	 * Saves a game and all related data into a zip file, returned as a byte array
	 */
	fun save(game: HungerGame): ByteArray {

		val output = ByteArrayOutputStream()

		ZipOutputStream(output).use { outputStream ->

			val gameJson = objectMapper().writeValueAsString(game)

			var zipEntry = ZipEntry("game.json")
			outputStream.putNextEntry(zipEntry)
			outputStream.write(gameJson.toByteArray(), 0, gameJson.toByteArray().size)
			outputStream.closeEntry()

			game.tributes.forEach { tribute ->
				zipEntry = ZipEntry("tributes/${tribute.discordId}.png")
				outputStream.putNextEntry(zipEntry)
				ImageIO.write(tribute.image, "PNG", outputStream)
				outputStream.closeEntry()
			}

			fileService.readFilesInDirectory("data", "other").forEach {
				zipEntry = ZipEntry("data/other/${it.key.fileName}")
				outputStream.putNextEntry(zipEntry)
				outputStream.write(it.value.toByteArray(), 0, it.value.toByteArray().size)
				outputStream.closeEntry()
			}

			fileService.readFilesInDirectory("data", "events").forEach {
				zipEntry = ZipEntry("data/events/${it.key.fileName}")
				outputStream.putNextEntry(zipEntry)
				outputStream.write(it.value.toByteArray(), 0, it.value.toByteArray().size)
				outputStream.closeEntry()
			}

			fileService.readFilesInDirectory("data", "events", "day").forEach {
				zipEntry = ZipEntry("data/events/day/${it.key.fileName}")
				outputStream.putNextEntry(zipEntry)
				outputStream.write(it.value.toByteArray(), 0, it.value.toByteArray().size)
				outputStream.closeEntry()
			}

			fileService.readFilesInDirectory("data", "events", "night").forEach {
				zipEntry = ZipEntry("data/events/night/${it.key.fileName}")
				outputStream.putNextEntry(zipEntry)
				outputStream.write(it.value.toByteArray(), 0, it.value.toByteArray().size)
				outputStream.closeEntry()
			}

			fileService.readFilesInDirectory("data", "events", "arena").forEach {
				zipEntry = ZipEntry("data/events/arena/${it.key.fileName}")
				outputStream.putNextEntry(zipEntry)
				outputStream.write(it.value.toByteArray(), 0, it.value.toByteArray().size)
				outputStream.closeEntry()
			}

			fileService.readFilesInDirectory("data", "items").forEach {
				zipEntry = ZipEntry("data/items/${it.key.fileName}")
				outputStream.putNextEntry(zipEntry)
				outputStream.write(it.value.toByteArray(), 0, it.value.toByteArray().size)
				outputStream.closeEntry()
			}

		}

		return output.toByteArray()

	}

	fun load(byteArray: ByteArray): HungerGame {

		val files = mutableMapOf<String, ByteArray>()

		ZipInputStream(ByteArrayInputStream(byteArray)).use { inputStream ->

			var entry = inputStream.nextEntry

			while (entry != null) {

				log.debug(entry.name)

				files += entry.name to inputStream.readAllBytes()

				entry = inputStream.nextEntry

			}

		}

		val events = mutableListOf<Event>()

		files.entries.filter { it.key.startsWith("data/events") }.forEach {
			events += when {
				it.key.startsWith("data/events/day/") -> {
					eventLoaderService.loadEventArray(String(it.value), "day")
				}
				it.key.startsWith("data/events/night/") -> {
					eventLoaderService.loadEventArray(String(it.value), "night")
				}
				else -> {
					eventLoaderService.loadEventArray(String(it.value))
				}
			}
		}

		log.debug(events.size)
		log.debug(events)

		val items = mutableListOf<Item>()

		files.entries.filter { it.key.startsWith("data/items") }.forEach {
			items += itemLoaderService.loadItemArray(String(it.value))
		}

		log.debug(items.size)
		log.debug(items)

		val arenaEvents = mutableListOf<ArenaEvent>()

		files.entries.filter { it.key.startsWith("data/other/arenaEvents.json") }.forEach {
			arenaEvents += eventLoaderService.loadArenaEventArray(String(it.value))
		}

		log.debug(arenaEvents.size)
		log.debug(arenaEvents)

		val game = objectMapper().readValue(String(files.entries.single { it.key == "game.json" }.value), HungerGame::class.java)

		files.entries.filter { it.key.startsWith("tributes/") }.forEach { file ->

			val discordId = file.key.substringAfter("/").substringBeforeLast(".").toLong()

			game.tributes.single { it.discordId == discordId }.image = ImageIO.read(ByteArrayInputStream(file.value))

		}

		game.setEvents(events)
		game.setArenaEvents(arenaEvents)
		game.setItems(items)

		game.replay()

		log.debug(game)

		return game

	}

	private fun objectMapper(): ObjectMapper {

		return jacksonObjectMapper().apply {

			registerModule(SimpleModule().apply {

				addSerializer(object : StdSerializer<Map<Item, Int>>(Map::class.java, true) {

					override fun serialize(value: Map<Item, Int>, gen: JsonGenerator, serializers: SerializerProvider?) {
						gen.writeStartArray()
						value.entries.forEach { entry ->
							gen.writeStartObject()
							gen.writeStringField("name", entry.key.name)
							gen.writeStringField("plural", entry.key.plural)
							gen.writeStringField("article", entry.key.article)
							gen.writeArrayFieldStart("tags")
							entry.key.tags.forEach { tag ->
								gen.writeString(tag)
							}
							gen.writeEndArray()
							gen.writeNumberField("quantity", entry.value)
							gen.writeEndObject()
						}
						gen.writeEndArray()
					}

				})

				addDeserializer(Map::class.java, object : StdDeserializer<Map<Item, Int>>(Map::class.java) {

					override fun deserialize(p: JsonParser, ctxt: DeserializationContext?): Map<Item, Int> {

						val node = p.codec.readTree<JsonNode>(p)
						val output = mutableMapOf<Item, Int>()

						node.forEach { item ->
							val name = item["name"].asText()
							val plural = item["plural"].asText()
							val article = item["article"].asText()
							val tags = item["tags"].map { it.asText() }
							output += Item(name, plural, article, tags) to item["quantity"].asInt()
						}

						return output
					}

				})

			})

		}

	}

}
