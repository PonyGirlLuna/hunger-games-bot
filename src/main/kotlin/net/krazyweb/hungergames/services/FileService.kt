package net.krazyweb.hungergames.services

import net.krazyweb.util.BotProperties
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import java.awt.image.BufferedImage
import java.nio.file.Files
import java.nio.file.Path
import javax.imageio.ImageIO

class FileService(private val properties: BotProperties) {

	val log: Logger = LogManager.getLogger(FileService::class.java)

	fun readImage(vararg path: String): BufferedImage {

		val finalPath = properties.getPathProperty("workingDirectory").let { basePath ->
			var output = basePath
			path.forEach {
				output = output.resolve(it)
			}
			output
		}

		return ImageIO.read(finalPath.toFile())

	}

	fun readAllLines(vararg path: String): String {

		val finalPath = properties.getPathProperty("workingDirectory").let { basePath ->
			var output = basePath
			path.forEach {
				output = output.resolve(it)
			}
			output
		}

		return Files.readAllLines(finalPath).joinToString("\n")

	}

	fun readAllFromDirectory(vararg path: String): List<String> {

		log.debug("Using working directory: ${properties.getPathProperty("workingDirectory").toAbsolutePath()}")

		val finalPath = properties.getPathProperty("workingDirectory").let { basePath ->
			var output = basePath
			path.forEach {
				output = output.resolve(it)
			}
			output
		}

		if (Files.notExists(finalPath)) {
			return emptyList()
		}

		return listFilesInDirectory(finalPath).map {
			Files.readAllLines(it).joinToString("\n")
		}

	}

	fun readFilesInDirectory(vararg path: String): Map<Path, String> {

		val finalPath = properties.getPathProperty("workingDirectory").let { basePath ->
			var output = basePath
			path.forEach {
				output = output.resolve(it)
			}
			output
		}

		if (Files.notExists(finalPath)) {
			return mapOf()
		}

		return listFilesInDirectory(finalPath).map {
			it to Files.readAllLines(it).joinToString("\n")
		}.toMap()

	}

	private fun listFilesInDirectory(directory: Path): List<Path> {
		return Files.newDirectoryStream(directory).use { directoryStream ->
			directoryStream.filter { !Files.isDirectory(it) }
		}
	}

}
