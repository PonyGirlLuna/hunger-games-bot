package net.krazyweb.hungergames.services

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.treeToValue
import net.krazyweb.hungergames.data.DeathType
import net.krazyweb.hungergames.data.events.*

class EventFormatException(message: String) : Exception(message)

class EventLoaderService(private val fileService: FileService, private val pronounService: PronounService, private val preferenceService: PlayerPreferenceService) {

	fun loadAllEvents(): List<Event> {

		val allEvents: MutableList<Event> = mutableListOf()

		allEvents += fileService.readAllFromDirectory("data", "events").map { loadEventArray(it) }.flatten()
		allEvents += fileService.readAllFromDirectory("data", "events", "day").map { loadEventArray(it, "day") }.flatten()
		allEvents += fileService.readAllFromDirectory("data", "events", "night").map { loadEventArray(it, "night") }.flatten()
		allEvents += fileService.readAllFromDirectory("data", "events", "arena").map { loadEventArray(it) }.flatten()

		return allEvents

	}

	fun loadAllArenaEvents(): List<ArenaEvent> {
		return loadArenaEventArray(fileService.readAllLines("data", "other", "arenaEvents.json"))
	}

	fun loadArenaEventArray(array: String): List<ArenaEvent> {
		return jacksonObjectMapper().readValue(array, object : TypeReference<List<ArenaEvent>>() {})
	}

	fun loadEventArray(event: String, timeOfDay: String = "any"): List<Event> {

		val eventsInFile = jacksonObjectMapper().readTree(event)

		return eventsInFile.map { loadEventArray(it, timeOfDay) }

	}

	private fun loadEventArray(json: JsonNode, timeOfDay: String): Event {

		val objectMapper = jacksonObjectMapper()

		if (!json.has("message")) {
			throw EventFormatException("Event does not contain a 'message' field.")
		}

		val eventConstructors = EventMessageParser(pronounService, preferenceService).parse(json["message"].asText())
		val numberOfTributes = getTotalNumberOfTributes(eventConstructors)

		if (!areIdsSequential(getTributeIds(eventConstructors))) {
			throw EventFormatException("Tribute IDs must not skip numbers (ex: 1, 3, 4, 2; not 1, 3, 4, 5). They are currently ${getTributeIds(eventConstructors).map { it + 1 }}.")
		}

		val timeOfDayRestriction = if (json.has("timeOfDay")) {
			json["timeOfDay"].asText()
		} else {
			timeOfDay
		}

		if (timeOfDayRestriction !in listOf("day", "night", "any")) {
			throw EventFormatException("'timeOfDay' is '$timeOfDayRestriction'. It must be one of ['day', 'night', 'any'] or be omitted.")
		}

		val eventRestriction = if (json.has("event")) {
			json["event"].asText()
		} else {
			null
		}

		val killers = if (json.has("killers") || json.has("killer")) {
			if (json.has("killers")) {
				if (json["killers"].isValueNode) {
					listOf(json["killers"].asInt())
				} else {
					objectMapper.treeToValue(json["killers"])
				}
			} else {
				listOf(json["killer"].asInt())
			}
		} else {
			listOf()
		}.map { it - 1 }

		if (killers.any { it > numberOfTributes - 1 }) {
			throw EventFormatException("'killers' references more tributes than are defined in the 'message' field ($numberOfTributes).")
		}

		val killed = if (json.has("killed")) {
			if (json["killed"].isValueNode) {
				listOf(json["killed"].asInt())
			} else {
				objectMapper.treeToValue(json["killed"])
			}
		} else {
			listOf()
		}.map { it - 1 }

		if (killed.any { it > numberOfTributes - 1 }) {
			throw EventFormatException("'killed' references more tributes than are defined in the 'message' field ($numberOfTributes).")
		}

		if (killers.isNotEmpty() && killed.isEmpty()) {
			throw EventFormatException("'killed' cannot be empty when 'killers' is not.")
		}

		val deathType = if (json.has("deathType")) {
			when (json["deathType"].asText().toLowerCase()) {
				"murder" -> DeathType.MURDER
				"accidental" -> DeathType.ACCIDENTAL
				"intentional" -> DeathType.INTENTIONAL
				"environmental" -> DeathType.ENVIRONMENTAL
				else -> throw EventFormatException("Unknown deathType. Valid values are: `murder`, `accidental`, `intentional`, `environmental`.")
			}
		} else {
			null
		}

		if (killed.isNotEmpty() && deathType == null) {
			throw EventFormatException("A deathType must be provided when tributes are killed.")
		}

		if (killed.isEmpty() && deathType != null) {
			throw EventFormatException("A deathType has been provided, but no tributes are killed.")
		}

		if (killers.isEmpty() && deathType == DeathType.MURDER) {
			throw EventFormatException("deathType is murder, but no killers were specified.")
		}

		val itemReferences = if (json.has("items")) {
			json.get("items").map {

				if (!it.has("id")) {
					throw EventFormatException("Item reference is missing an id field.")
				}

				val id = it["id"].asInt() - 1

				val owner = if (it.has("owner")) {
					it["owner"].asInt() - 1
				} else {
					null
				}

				//TODO Test this (owner id is greater than the number of tributes)
				if (owner != null && owner >= numberOfTributes) {
					throw EventFormatException("Item owner '${owner + 1}' references a tribute that does not exist.")
				}

				val tags: List<String> = objectMapper.treeToValue(it["tags"])

				val quantity = if (it.has("quantity")) {
					it["quantity"].asInt()
				} else {
					1
				}

				ItemReference(id, owner, quantity, tags)

			}
		} else {
			listOf()
		}

		val inventoryChanges = if (json.has("inventory_changes")) {
			json.get("inventory_changes").mapIndexed { index, it ->

				val from = if (it.has("from")) {
					it["from"].asInt() - 1
				} else {
					null
				}

				val to = if (it.has("to")) {
					it["to"].asInt() - 1
				} else {
					null
				}

				val item = it["item"].asInt() - 1 //TODO Good error message for missing "item" field

				if (item >= itemReferences.size) {
					throw EventFormatException("inventory_change[$index] references items[$item], but 'items' only contains ${itemReferences.size} element(s).")
				}

				if (from != null && itemReferences[item].owner == null) {
					throw EventFormatException("inventory_change[$index] requires that Tribute ${from + 1} owns items[$item], but items[$item] is missing an 'owner' field.")
				}

				if (to != null && itemReferences[item].owner == to) {
					throw EventFormatException("inventory_change[$index] gives tribute ${to + 1} their own item! (Don't do this <3)")
				}

				InventoryChange(from, to, item)

			}
		} else {
			listOf()
		}

		if (itemReferences.isNotEmpty() && eventConstructors.filterIsInstance(ItemNameEventConstructor::class.java).none()) {
			throw EventFormatException("Event contains a reference to one or more items, but no item references were included in the message field.")
		}

		return Event(eventConstructors, timeOfDayRestriction, eventRestriction, killers, killed, deathType, itemReferences, inventoryChanges, json["message"].asText())

	}

	private fun getTotalNumberOfTributes(constructors: List<EventConstructor>): Int {
		return constructors.filterIsInstance<PlayerNameEventConstructor>().distinctBy { it.id }.count()
	}

	private fun areIdsSequential(ids: List<Int>): Boolean {

		var output = true
		var last = 0

		ids.sorted().forEach {
			if ((it + 1) - last > 1) {
				output = false
			}
			last = it + 1
		}

		return output

	}

	private fun getTributeIds(constructors: List<EventConstructor>): List<Int> {
		return constructors.filterIsInstance<PlayerNameEventConstructor>().distinctBy { it.id }.map { it.id }
	}

}
