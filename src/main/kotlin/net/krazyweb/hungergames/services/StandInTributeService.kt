package net.krazyweb.hungergames.services

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import net.krazyweb.hungergames.data.PlayerPreferenceKey
import net.krazyweb.hungergames.data.Tribute

data class StandInTribute(val name: String, val fakeDiscordId: Long, val pronouns: String)

class StandInTributeService(private val fileService: FileService, private val playerPreferenceService: PlayerPreferenceService) {

	fun getTributes(amount: Int): List<Tribute> {

		val standInTributes = jacksonObjectMapper().readValue<List<StandInTribute>>(fileService.readAllLines("data", "stand-in-tributes", "tributes.json"))
		return standInTributes.shuffled().take(amount).map {
			playerPreferenceService.save(it.fakeDiscordId, PlayerPreferenceKey.PRONOUNS, it.pronouns)
			Tribute(it.name, it.fakeDiscordId, false, fileService.readImage("data", "stand-in-tributes", "images", "${it.name}.png"))
		}

	}

}
