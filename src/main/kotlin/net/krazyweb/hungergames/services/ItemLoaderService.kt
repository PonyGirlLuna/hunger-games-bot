package net.krazyweb.hungergames.services

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import net.krazyweb.hungergames.data.Item

class ItemLoaderService(private val fileService: FileService) {

	fun loadAllItems(): List<Item> {

		val allItems: MutableList<Item> = mutableListOf()

		allItems += fileService.readAllFromDirectory("data", "items").map { loadItemArray(it) }.flatten()

		return allItems

	}

	fun loadItemArray(itemJson: String): List<Item> {
		return jacksonObjectMapper().readValue(itemJson, object : TypeReference<MutableList<Item>>(){})
	}

}
