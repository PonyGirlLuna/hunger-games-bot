package net.krazyweb.hungergames

import io.mockk.every
import net.krazyweb.hungergames.data.Item
import net.krazyweb.hungergames.data.Tribute
import net.krazyweb.hungergames.data.events.ArenaEvent
import net.krazyweb.hungergames.data.events.Event
import net.krazyweb.hungergames.game.ArenaData
import net.krazyweb.hungergames.game.ItemEventType
import net.krazyweb.hungergames.services.EventLoaderService
import nl.jqno.equalsverifier.EqualsVerifier
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import kotlin.random.Random
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class EventTest : BotTest() {

	@Test
	fun `canRunEvent with only a message`() {

		val event = createEvent("""
			[{
				"message": "1"
			}]
		""")

		testEvent(event) { tributes, fatal, itemEventType, _, _, activeEvent ->
			tributes.size == 1 && !fatal && itemEventType == ItemEventType.NONE && activeEvent == null
		}

	}

	@Test
	fun `canRunEvent with a fatality`() {

		val event = createEvent("""
			[{
				"message": "1",
				"killed": [1],
				"deathType": "environmental"
			}]
		""")

		testEvent(event) { tributes, fatal, itemEventType, _, _, activeEvent ->
			tributes.size == 1 && fatal && itemEventType == ItemEventType.NONE && activeEvent == null
		}

	}

	@Test
	fun `canRunEvent with a killer and a fatality`() {

		val event = createEvent("""
			[{
				"message": "1 2",
				"killers": [1],
				"killed": [2],
				"deathType": "murder"
			}]
		""")

		testEvent(event) { tributes, fatal, itemEventType, _, _, activeEvent ->
			tributes.size == 2 && fatal && itemEventType == ItemEventType.NONE && activeEvent == null
		}

	}

	@Test
	fun `canRunEvent requiring an arena event that is not fatal`() {

		val event = createEvent("""
			[{
				"message": "1 2",
				"event": "The Bloodbath"
			}]
		""")

		testEvent(event) { tributes, fatal, itemEventType, _, _, activeEvent ->
			tributes.size == 2 && !fatal && itemEventType == ItemEventType.NONE && activeEvent == "The Bloodbath"
		}

	}

	@Test
	fun `canRunEvent requiring an arena event`() {

		val event = createEvent("""
			[{
				"message": "1 2",
				"event": "The Bloodbath",
				"killers": 1,
				"killed": 2,
				"deathType": "murder"
			}]
		""")

		testEvent(event) { tributes, fatal, itemEventType, _, _, activeEvent ->
			tributes.size == 2 && fatal && itemEventType == ItemEventType.NONE && activeEvent == "The Bloodbath"
		}

	}

	@Test
	fun `canRunEvent requiring the day`() {

		val event = createEvent("""
			[{
				"message": "1 2 3 4 5",
				"timeOfDay": "day"
			}]
		""")

		testEvent(event) { tributes, fatal, itemEventType, _, time, activeEvent ->
			tributes.size == 5 && !fatal && itemEventType == ItemEventType.NONE && time == "day" && activeEvent == null
		}

	}

	@Test
	fun `canRunEvent requiring the night`() {

		val event = createEvent("""
			[{
				"message": "1",
				"timeOfDay": "night"
			}]
		""")

		testEvent(event) { tributes, fatal, itemEventType, _, time, activeEvent ->
			tributes.size == 1 && !fatal && itemEventType == ItemEventType.NONE && time == "night" && activeEvent == null
		}

	}

	@Test
	fun `canRunEvent requiring ten tributes`() {

		val event = createEvent("""
			[{
				"message": "1 2 3 4 5 6 7 8 9 10"
			}]
		""")

		testEvent(event) { tributes, fatal, itemEventType, _, _, activeEvent ->
			tributes.size == 10 && !fatal && itemEventType == ItemEventType.NONE && activeEvent == null
		}

	}

	@Test
	fun `canRunEvent requiring an item`() {

		val event = createEvent("""
			[{
				"message": "1 ${'$'}s1",
				"items": [
					{
						"id": 1,
						"owner": 1,
						"tags": ["any"]
					}
				]
			}]
		""")

		testEvent(event) { tributes, fatal, itemEventType, _, _, activeEvent ->
			tributes.size == 10 && !fatal && itemEventType == ItemEventType.USES_ITEM && activeEvent == null
		}

	}

	@Test
	fun `Event kills a tribute with no killer`() {

		val event = createEvent("""
			[{
				"message": "1",
				"killed": 1,
				"deathType": "environmental"
			}]
		""")

		val tribute = getTributes().subList(0, 1)

		event.runEvent(tribute)

		Assertions.assertFalse { tribute[0].alive }

	}

	@Test
	fun `Event kills a tribute with a killer`() {

		val event = createEvent("""
			[{
				"message": "1 2",
				"killed": 2,
				"killers": 1,
				"deathType": "murder"
			}]
		""")

		val tributes = getTributes().subList(0, 2)

		event.runEvent(tributes)

		Assertions.assertTrue { tributes[0].alive }
		Assertions.assertFalse { tributes[1].alive }

	}

	@Test
	fun `Event kills the correct tributes with lots of them mixed in`() {

		val event = createEvent("""
			[{
				"message": "1 2 3 4 5 6 7 8 9 10",
				"killed": [1, 3, 4, 6, 7, 9],
				"killers": [2, 5, 8, 10],
				"deathType": "murder"
			}]
		""")

		val tributes = getTributes().subList(0, 10)

		event.runEvent(tributes)

		Assertions.assertFalse { tributes[0].alive }
		Assertions.assertTrue { tributes[1].alive }
		Assertions.assertFalse { tributes[2].alive }
		Assertions.assertFalse { tributes[3].alive }
		Assertions.assertTrue { tributes[4].alive }
		Assertions.assertFalse { tributes[5].alive }
		Assertions.assertFalse { tributes[6].alive }
		Assertions.assertTrue { tributes[7].alive }
		Assertions.assertFalse { tributes[8].alive }
		Assertions.assertTrue { tributes[9].alive }

	}

	@Test
	fun `Event kills the correct tributes when some don't appear in either killed or killers`() {

		val event = createEvent("""
			[{
				"message": "1 2 3 4 5",
				"killed": [1, 3],
				"killers": [5],
				"deathType": "murder"
			}]
		""")

		val tributes = getTributes().subList(0, 10)

		event.runEvent(tributes)

		Assertions.assertFalse { tributes[0].alive }
		Assertions.assertTrue { tributes[1].alive }
		Assertions.assertFalse { tributes[2].alive }
		Assertions.assertTrue { tributes[3].alive }
		Assertions.assertTrue { tributes[4].alive }

	}

	@Test
	fun `Event adds items`() {

		val testItem = Item("a", "a", "a", listOf("a"))
		val items = mutableListOf(testItem)

		val event = createEvent("""
			[{
				"message": "1 ${'$'}s1",
				"items": [
					{
						"id": 1,
						"tags": ["any"]
					}
				],
				"inventory_changes": [
					{
						"item": 1,
						"to": 1
					}
				]
			}]
		""")

		val tributes = getTributes().take(1)

		event.runEvent(tributes, items, Random(1))

		assertTrue { tributes[0].inventory.contains(testItem) }

	}

	@Test
	fun `Event removes items`() {

		val testItem = Item("a", "a", "a", listOf("a"))
		val items = mutableListOf(testItem)

		val event = createEvent("""
			[{
				"message": "1 ${'$'}s1",
				"items": [
					{
						"id": 1,
						"owner": 1,
						"tags": ["any"]
					}
				],
				"inventory_changes": [
					{
						"item": 1,
						"from": 1
					}
				]
			}]
		""")

		val tributes = getTributes().take(1)
		tributes[0].addItem(testItem, 1)

		event.runEvent(tributes, items, Random(1))

		assertFalse { tributes[0].inventory.contains(testItem) }

	}

	@Test
	fun `Event moves items to another tribute`() {

		val testItem = Item("a", "a", "a", listOf("a"))
		val items = mutableListOf(testItem)

		val event = createEvent("""
			[{
				"message": "1 2 ${'$'}s1",
				"items": [
					{
						"id": 1,
						"owner": 1,
						"tags": ["any"]
					}
				],
				"inventory_changes": [
					{
						"item": 1,
						"from": 1,
						"to": 2
					}
				]
			}]
		""")

		val tributes = getTributes().take(2)
		tributes[0].addItem(testItem, 1)

		event.runEvent(tributes, items, Random(1))

		assertFalse { tributes[0].inventory.contains(testItem) }
		assertTrue { tributes[1].inventory.contains(testItem) }

	}

	@Test
	fun `Tribute has item with required tag`() {

		val testItem = Item("a", "a", "a", listOf("weapon"))

		val event = createEvent("""
			[{
				"message": "1 2 ${'$'}s1",
				"items": [
					{
						"id": 1,
						"owner": 1,
						"tags": ["weapon"]
					}
				]
			}]
		""")

		val tributes = getTributes().take(2)
		tributes[0].addItem(testItem, 1)

		assertTrue { event.canRunEvent(tributes, false, itemEventType = ItemEventType.USES_ITEM, ArenaData(1, "day", null)) }

	}

	@Test
	fun `Tribute does not have item with required tag`() {

		val testItem = Item("a", "a", "a", listOf("weapon"))

		val event = createEvent("""
			[{
				"message": "1 2 ${'$'}s1",
				"items": [
					{
						"id": 1,
						"owner": 1,
						"tags": ["edible"]
					}
				]
			}]
		""")

		val tributes = getTributes().take(2)
		tributes[0].addItem(testItem, 1)

		assertFalse { event.canRunEvent(tributes, false, itemEventType = ItemEventType.USES_ITEM, ArenaData(1, "day", null)) }

	}

	@Test
	fun `canRunEvent requiring item to be used`() {

		val testItem = Item("a", "a", "a", listOf("edible"))

		val event = createEvent("""
			[{
				"message": "1 ${'$'}s1",
				"items": [
					{
						"id": 1,
						"owner": 1,
						"tags": ["edible"]
					}
				]
			}]
		""")

		val tributes = getTributes().take(1)
		tributes[0].addItem(testItem, 1)

		assertTrue { event.canRunEvent(tributes, false, itemEventType = ItemEventType.USES_ITEM, ArenaData(1, "day", null)) }
		assertFalse { event.canRunEvent(tributes, false, itemEventType = ItemEventType.NONE, ArenaData(1, "day", null)) }
		assertFalse { event.canRunEvent(tributes, false, itemEventType = ItemEventType.GAINS_ITEM, ArenaData(1, "day", null)) }

	}

	@Test
	fun `canRunEvent requiring item to be gained`() {

		val testItem = Item("a", "a", "a", listOf("edible"))

		val event = createEvent("""
			[{
				"message": "1 ${'$'}s1",
				"items": [
					{
						"id": 1,
						"tags": ["edible"]
					}
				],
				"inventory_changes": [
					{
						"item": 1,
						"to": 1
					}
				]
			}]
		""")

		val tributes = getTributes().take(1)
		tributes[0].addItem(testItem, 1)

		assertFalse { event.canRunEvent(tributes, false, itemEventType = ItemEventType.USES_ITEM, ArenaData(1, "day", null)) }
		assertFalse { event.canRunEvent(tributes, false, itemEventType = ItemEventType.NONE, ArenaData(1, "day", null)) }
		assertTrue { event.canRunEvent(tributes, false, itemEventType = ItemEventType.GAINS_ITEM, ArenaData(1, "day", null)) }

	}

	@Test
	fun `Test equals`() {

		EqualsVerifier
				.simple()
				.forClass(Event::class.java)
				.withNonnullFields("constructors", "timeOfDay", "killers", "killed", "itemReferences", "inventoryChanges", "messageTemplate")
				.withIgnoredFields("messageTemplate")
				.verify()

	}

	private fun testEvent(event: Event, predicate: (tributes: List<Tribute>, fatal: Boolean, itemEventType: ItemEventType, day: Int, time: String, activeEvent: String?) -> Boolean) {
		return testEvent(getTributes(), event, predicate)
	}

	private fun testEvent(allTributes: List<Tribute>, event: Event, predicate: (tributes: List<Tribute>, fatal: Boolean, itemEventType: ItemEventType, day: Int, time: String, activeEvent: String?) -> Boolean) {

		for (i in 0 until 10) {

			val tributes = allTributes.subList(0, i)

			for (time in listOf("day", "night")) {
				for (activeEvent in listOf(null, ArenaEvent("The Bloodbath", "day"), ArenaEvent("Hurricane", "day"))) {
					for (day in 1..10) {
						for (fatal in listOf(true, false)) {
							for (itemEventType in listOf(ItemEventType.USES_ITEM, ItemEventType.GAINS_ITEM, ItemEventType.NONE)) {

								if (predicate(tributes, fatal, itemEventType, day, time, activeEvent?.name)) {
									assertTrue(event.canRunEvent(tributes, fatal, itemEventType, ArenaData(day, time, activeEvent)))
								} else {
									assertFalse(event.canRunEvent(tributes, fatal, itemEventType, ArenaData(day, time, activeEvent)))
								}

							}
						}
					}
				}
			}

		}

	}

	private fun createEvent(event: String): Event {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf(event.trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
		return eventLoaderService.loadAllEvents().first()

	}

}
