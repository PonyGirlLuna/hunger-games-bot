package net.krazyweb.hungergames

import io.mockk.every
import io.mockk.mockk
import net.krazyweb.hungergames.data.messages.GameOverMessage
import net.krazyweb.hungergames.data.messages.Message
import net.krazyweb.hungergames.data.messages.Subscriber
import net.krazyweb.hungergames.services.*
import net.krazyweb.util.BotProperties
import java.nio.file.Files
import java.nio.file.Paths

fun main() {

	val file = Paths.get("")
	val gameData = Files.readAllBytes(file)

	val properties = mockk<BotProperties>()
	every { properties.getPathProperty("workingDirectory") } returns Paths.get("")


	val gamePersistenceService = GamePersistenceService(FileService(properties), EventLoaderService(FileService(properties), PronounService(properties), PlayerPreferenceService(properties)), ItemLoaderService(FileService(properties)))
	val hungerGame = gamePersistenceService.load(gameData)

	var gameFinished = false

	hungerGame.addSubscriber(object : Subscriber {
		override fun call(message: Message) {
			when (message) {
				is GameOverMessage -> {
					gameFinished = true
				}
				else -> println(message)
			}
		}
	})

	hungerGame.replay()

	while (!gameFinished) {
		hungerGame.next()
	}

	println("Finished hunger game!")

}
