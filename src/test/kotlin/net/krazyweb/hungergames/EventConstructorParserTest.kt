package net.krazyweb.hungergames

import net.krazyweb.hungergames.data.Item
import net.krazyweb.hungergames.data.events.*
import nl.jqno.equalsverifier.EqualsVerifier
import org.junit.jupiter.api.Test
import kotlin.random.Random
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class EventConstructorParserTest : BotTest() {

	@Test
	fun `Test a message with just a player name`() {

		val message = "1 draws a pony in the dirt with a stick."
		val tributes = getTributes()

		val event = Event(EventMessageParser(getMockPronounService(), getMockPlayerPreferenceService()).parse(message), messageTemplate = message)

		assertEquals("**Applejack** draws a pony in the dirt with a stick.", event.runEvent(tributes))

	}

	@Test
	fun `Test a message with a player name and a pronoun`() {

		val message = "1 draws a pony in the dirt with (their/1) stick."
		val tributes = getTributes()

		val event = Event(EventMessageParser(getMockPronounService(), getMockPlayerPreferenceService()).parse(message), messageTemplate = message)

		assertEquals("**Applejack** draws a pony in the dirt with her stick.", event.runEvent(tributes))

	}

	@Test
	fun `Test a message with two player names`() {

		val message = "1 draws a pony in the dirt with a stick while 2 watches."
		val tributes = getTributes()

		val event = Event(EventMessageParser(getMockPronounService(), getMockPlayerPreferenceService()).parse(message), messageTemplate = message)

		assertEquals("**Applejack** draws a pony in the dirt with a stick while **Trixie** watches.", event.runEvent(tributes))

	}

	@Test
	fun `Test a message with a player name before punctuation`() {

		val message = "1 draws a pony in the dirt with a stick while 2, 3, and 4 watch."
		val tributes = getTributes()

		val event = Event(EventMessageParser(getMockPronounService(), getMockPlayerPreferenceService()).parse(message), messageTemplate = message)

		assertEquals("**Applejack** draws a pony in the dirt with a stick while **Trixie**, **Trouble Shoes**, and **Philomena** watch.", event.runEvent(tributes))

	}

	@Test
	fun `Test a message with a player name before all types of punctuation`() {

		val message = "1. 1, 1! 1? 1'"
		val tributes = getTributes()

		val event = Event(EventMessageParser(getMockPronounService(), getMockPlayerPreferenceService()).parse(message), messageTemplate = message)

		assertEquals("**Applejack**. **Applejack**, **Applejack**! **Applejack**? **Applejack**'", event.runEvent(tributes))

	}

	@Test
	fun `Test a message with a player name with punctuation and no spacing`() {

		val message = "1.1,1!1?1'"
		val tributes = getTributes()

		val event = Event(EventMessageParser(getMockPronounService(), getMockPlayerPreferenceService()).parse(message), messageTemplate = message)

		assertEquals("**Applejack**.**Applejack**,**Applejack**!**Applejack**?**Applejack**'", event.runEvent(tributes))

	}

	@Test
	fun `Test a message with multiple player names with punctuation and varied spacing`() {

		val message = "2. 1,2! 1?2'"
		val tributes = getTributes()

		val event = Event(EventMessageParser(getMockPronounService(), getMockPlayerPreferenceService()).parse(message), messageTemplate = message)

		assertEquals("**Trixie**. **Applejack**,**Trixie**! **Applejack**?**Trixie**'", event.runEvent(tributes))

	}

	@Test
	fun `Test a message with multiple pronouns`() {

		val message = "1 flails (their/1) Cardboard Seer Buddy™ around, accidentally killing 2. 1 takes (their/2) explosives."
		val tributes = getTributes()

		val event = Event(EventMessageParser(getMockPronounService(), getMockPlayerPreferenceService()).parse(message), messageTemplate = message)

		assertEquals("**Applejack** flails her Cardboard Seer Buddy™ around, accidentally killing **Trixie**. **Applejack** takes her explosives.", event.runEvent(tributes))

	}

	@Test
	fun `Test a message with lots of pronouns`() {

		val message = "1 (they/1) 2 (them/2) 3 (their/3) 4 (theirs/4) 5 (themselves/5)."
		val tributes = getTributes()

		val event = Event(EventMessageParser(getMockPronounService(), getMockPlayerPreferenceService()).parse(message), messageTemplate = message)

		assertEquals("**Applejack** she **Trixie** her **Trouble Shoes** his **Philomena** theirs **Princess Celestia** herself.", event.runEvent(tributes))

	}

	@Test
	fun `Test a message with pronouns and various spacings`() {

		val message = "1(they/1)2 (them/2) 3(their/3) 4 (theirs/4)5 (themselves/5)."
		val tributes = getTributes()

		val event = Event(EventMessageParser(getMockPronounService(), getMockPlayerPreferenceService()).parse(message), messageTemplate = message)

		assertEquals("**Applejack**she**Trixie** her **Trouble Shoes**his **Philomena** theirs**Princess Celestia** herself.", event.runEvent(tributes))

	}

	@Test
	fun `Test a message with capitalized pronouns`() {

		val message = "1(They/1)."
		val tributes = getTributes()

		val event = Event(EventMessageParser(getMockPronounService(), getMockPlayerPreferenceService()).parse(message), messageTemplate = message)

		assertEquals("**Applejack**She.", event.runEvent(tributes))

	}

	@Test
	fun `Test a message with following text`() {

		val message = "1 (They/1/run/runs). 2 (They/2/run/runs). 3 (They/3/run/runs). 4 (They/4/run/runs)."
		val tributes = getTributes()

		val event = Event(EventMessageParser(getMockPronounService(), getMockPlayerPreferenceService()).parse(message), messageTemplate = message)

		assertEquals("**Applejack** She runs. **Trixie** She runs. **Trouble Shoes** He runs. **Philomena** They run.", event.runEvent(tributes))

	}

	@Test
	fun `Test PronounEventConstructor equals`() {

		EqualsVerifier
				.simple()
				.forClass(PronounEventConstructor::class.java)
				.withNonnullFields("pronounType", "id", "followingTextPlural", "followingTextSingular", "capitalized", "pronounService", "playerPreferenceService")
				.withIgnoredFields("pronounService", "playerPreferenceService")
				.verify()

	}

	@Test
	fun `Test a message with verb conjugation`() {

		val message = "1 kills 2 as (they/2/try/tries) to run."
		val tributes = getTributes()

		val event = Event(EventMessageParser(getMockPronounService(), getMockPlayerPreferenceService()).parse(message), messageTemplate = message)

		assertEquals("**Applejack** kills **Trixie** as she tries to run.", event.runEvent(tributes))

	}

	@Test
	fun `Test a message with the other verb conjugation`() {

		val message = "1, 2, and 3 kill 4 as (they/4/try/tries) to run."
		val tributes = getTributes()

		val event = Event(EventMessageParser(getMockPronounService(), getMockPlayerPreferenceService()).parse(message), messageTemplate = message)

		assertEquals("**Applejack**, **Trixie**, and **Trouble Shoes** kill **Philomena** as they try to run.", event.runEvent(tributes))

	}

	@Test
	fun `Test a message with an item`() {

		val message = "1 grabs \$s1."
		val tributes = getTributes()

		val items = listOf(
				Item("grenade", "grenades", "a", listOf("explosive")),
				Item("apple", "apples", "an", listOf("edible"))
		)

		val event = Event(EventMessageParser(getMockPronounService(), getMockPlayerPreferenceService()).parse(message),
				itemReferences = listOf(ItemReference(0, null, tags = listOf("explosive"))),
				inventoryChanges = listOf(InventoryChange(null, 0, 0)),
				messageTemplate = message
		)

		assertEquals("**Applejack** grabs a **grenade**.", event.runEvent(tributes, items, Random(1)))
		assertTrue { tributes[0].inventory.containsKey(items[0]) }

	}

	@Test
	fun `Test a message with multiples of one item`() {

		val message = "1 grabs several \$p1."
		val tributes = getTributes()

		val items = listOf(
				Item("grenade", "grenades", "a", listOf("explosive"))
		)

		val event = Event(EventMessageParser(getMockPronounService(), getMockPlayerPreferenceService()).parse(message),
				itemReferences = listOf(ItemReference(0, quantity = 3, tags = listOf("explosive"))),
				inventoryChanges = listOf(InventoryChange(null, 0, 0)),
				messageTemplate = message
		)

		assertEquals("**Applejack** grabs several **grenades**.", event.runEvent(tributes, items, Random(1)))
		assertTrue { tributes[0].inventory.containsKey(items[0]) }
		assertEquals(3, tributes[0].inventory[items[0]])

	}

	@Test
	fun `Test a message with the singular form of one item`() {

		val message = "1 \$s1."
		val tributes = getTributes()

		val items = listOf(
				Item("apple", "apples", "an", listOf("edible"))
		)

		val event = Event(EventMessageParser(getMockPronounService(), getMockPlayerPreferenceService()).parse(message),
				itemReferences = listOf(ItemReference(0, quantity = 1, tags = listOf("any"))),
				messageTemplate = message
		)

		assertEquals("**Applejack** an **apple**.", event.runEvent(tributes, items, Random(1)))

	}

	@Test
	fun `Test a message with the plural form of one item`() {

		val message = "1 \$p1."
		val tributes = getTributes()

		val items = listOf(
				Item("apple", "apples", "an", listOf("edible"))
		)

		val event = Event(EventMessageParser(getMockPronounService(), getMockPlayerPreferenceService()).parse(message),
				itemReferences = listOf(ItemReference(0, quantity = 1, tags = listOf("any"))),
				messageTemplate = message
		)

		assertEquals("**Applejack** **apples**.", event.runEvent(tributes, items, Random(1)))

	}

	@Test
	fun `Test a message with the singular non-article form of one item`() {

		val message = "1 \$n1."
		val tributes = getTributes()

		val items = listOf(
				Item("apple", "apples", "an", listOf("edible"))
		)

		val event = Event(EventMessageParser(getMockPronounService(), getMockPlayerPreferenceService()).parse(message),
				itemReferences = listOf(ItemReference(0, quantity = 1, tags = listOf("any"))),
				messageTemplate = message
		)

		assertEquals("**Applejack** **apple**.", event.runEvent(tributes, items, Random(1)))

	}

	@Test
	fun `Test a message with the singular form of one item with a specific tag`() {

		val message = "1 \$s1."
		val tributes = getTributes()

		val items = listOf(
				Item("grenade", "grenades", "a", listOf("explosive")),
				Item("apple", "apples", "an", listOf("edible"))
		)

		val event = Event(EventMessageParser(getMockPronounService(), getMockPlayerPreferenceService()).parse(message),
				itemReferences = listOf(ItemReference(0, quantity = 1, tags = listOf("edible"))),
				messageTemplate = message
		)

		assertEquals("**Applejack** an **apple**.", event.runEvent(tributes, items, Random(1)))

	}

	@Test
	fun `Test a message with the plural form of one item with a specific tag`() {

		val message = "1 \$p1."
		val tributes = getTributes()

		val items = listOf(
				Item("grenade", "grenades", "a", listOf("explosive")),
				Item("apple", "apples", "an", listOf("edible"))
		)

		val event = Event(EventMessageParser(getMockPronounService(), getMockPlayerPreferenceService()).parse(message),
				itemReferences = listOf(ItemReference(0, quantity = 1, tags = listOf("edible"))),
				messageTemplate = message
		)

		assertEquals("**Applejack** **apples**.", event.runEvent(tributes, items, Random(1)))

	}

	@Test
	fun `Test a message with the singular non-article form of one item with a specific tag`() {

		val message = "1 \$n1."
		val tributes = getTributes()

		val items = listOf(
				Item("grenade", "grenades", "a", listOf("explosive")),
				Item("apple", "apples", "an", listOf("edible"))
		)

		val event = Event(EventMessageParser(getMockPronounService(), getMockPlayerPreferenceService()).parse(message),
				itemReferences = listOf(ItemReference(0, quantity = 1, tags = listOf("edible"))),
				messageTemplate = message
		)

		assertEquals("**Applejack** **apple**.", event.runEvent(tributes, items, Random(1)))

	}

}
