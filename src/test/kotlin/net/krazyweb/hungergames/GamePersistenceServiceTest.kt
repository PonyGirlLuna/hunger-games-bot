package net.krazyweb.hungergames

import io.mockk.every
import io.mockk.mockk
import net.krazyweb.hungergames.data.messages.GameOverMessage
import net.krazyweb.hungergames.data.messages.Message
import net.krazyweb.hungergames.data.messages.Subscriber
import net.krazyweb.hungergames.game.DeadlinessFactor
import net.krazyweb.hungergames.game.HungerGame
import net.krazyweb.hungergames.services.EventLoaderService
import net.krazyweb.hungergames.services.FileService
import net.krazyweb.hungergames.services.GamePersistenceService
import net.krazyweb.hungergames.services.ItemLoaderService
import net.krazyweb.util.BotProperties
import org.junit.jupiter.api.RepeatedTest
import java.nio.file.Paths
import kotlin.math.abs
import kotlin.math.floor
import kotlin.math.min
import kotlin.test.assertEquals

class GamePersistenceServiceTest : BotTest() {

	@RepeatedTest(25)
	fun `An un-started game is saved and loaded to the same state`() {

		val game = getAGame()
		val properties = getMockProperties()

		val service = GamePersistenceService(FileService(properties), EventLoaderService(FileService(properties), getMockPronounService(), getMockPlayerPreferenceService()), ItemLoaderService(FileService(properties)))
		val savedGame = service.save(game)

		val loadedGame = service.load(savedGame)

		assertEquals(game, loadedGame)

		game.start()
		loadedGame.start()

		assertEquals(game, loadedGame)

	}

	@RepeatedTest(25)
	fun `A started game is saved and loaded to the same state`() {

		val game = getAGame()
		game.start()

		val properties = getMockProperties()

		val service = GamePersistenceService(FileService(properties), EventLoaderService(FileService(properties), getMockPronounService(), getMockPlayerPreferenceService()), ItemLoaderService(FileService(properties)))
		val savedGame = service.save(game)

		val loadedGame = service.load(savedGame)

		assertEquals(game, loadedGame)

	}

	@RepeatedTest(25)
	fun `A loaded started game can be continued to completion`() {

		val game = getAGame()
		game.start()

		val properties = getMockProperties()

		val service = GamePersistenceService(FileService(properties), EventLoaderService(FileService(properties), getMockPronounService(), getMockPlayerPreferenceService()), ItemLoaderService(FileService(properties)))
		val savedGame = service.save(game)

		val loadedGame = service.load(savedGame)

		assertEquals(game, loadedGame)

		var gameFinished = false

		loadedGame.addSubscriber(object : Subscriber {
			override fun call(message: Message) {
				when (message) {
					is GameOverMessage -> {
						gameFinished = true
					}
				}
			}
		})

		while (!gameFinished) {
			loadedGame.next()
		}

		gameFinished = false

		game.addSubscriber(object : Subscriber {
			override fun call(message: Message) {
				when (message) {
					is GameOverMessage -> {
						gameFinished = true
					}
				}
			}
		})

		while (!gameFinished) {
			game.next()
		}

		assertEquals(game, loadedGame)

	}

	@RepeatedTest(25)
	fun `An in-progress game can be loaded`() {

		val game = getAGame()
		game.start()

		var moves = 10
		while (moves-- > 0) {
			game.next()
		}

		val properties = getMockProperties()

		val service = GamePersistenceService(FileService(properties), EventLoaderService(FileService(properties), getMockPronounService(), getMockPlayerPreferenceService()), ItemLoaderService(FileService(properties)))
		val savedGame = service.save(game)

		val loadedGame = service.load(savedGame)

		assertEquals(game, loadedGame)

	}

	@RepeatedTest(25)
	fun `An in-progress game can be loaded and continued to completion`() {

		val game = getAGame()
		game.start()

		var moves = 10
		while (moves-- > 0) {
			game.next()
		}

		val properties = getMockProperties()

		val service = GamePersistenceService(FileService(properties), EventLoaderService(FileService(properties), getMockPronounService(), getMockPlayerPreferenceService()), ItemLoaderService(FileService(properties)))
		val savedGame = service.save(game)

		val loadedGame = service.load(savedGame)

		assertEquals(game, loadedGame)

		var gameFinished = false

		loadedGame.addSubscriber(object : Subscriber {
			override fun call(message: Message) {
				when (message) {
					is GameOverMessage -> {
						gameFinished = true
					}
				}
			}
		})

		while (!gameFinished) {
			loadedGame.next()
		}

		gameFinished = false

		game.addSubscriber(object : Subscriber {
			override fun call(message: Message) {
				when (message) {
					is GameOverMessage -> {
						gameFinished = true
					}
				}
			}
		})

		while (!gameFinished) {
			game.next()
		}

		assertEquals(game, loadedGame)

	}

	@RepeatedTest(25)
	fun `A completed game can be loaded correctly`() {

		val game = getAGame()
		game.start()

		var gameFinished = false

		game.addSubscriber(object : Subscriber {
			override fun call(message: Message) {
				when (message) {
					is GameOverMessage -> {
						gameFinished = true
					}
				}
			}
		})

		while (!gameFinished) {
			game.next()
		}

		val properties = getMockProperties()

		val service = GamePersistenceService(FileService(properties), EventLoaderService(FileService(properties), getMockPronounService(), getMockPlayerPreferenceService()), ItemLoaderService(FileService(properties)))
		val savedGame = service.save(game)

		val loadedGame = service.load(savedGame)

		assertEquals(game, loadedGame)

		loadedGame.next()

		assertEquals(game, loadedGame)

	}

	private fun getAGame(seed: Long = System.currentTimeMillis()): HungerGame {

		val properties = getMockProperties()
		val tributes = getTributes()

		val eventLoaderService = EventLoaderService(FileService(properties), getMockPronounService(), getMockPlayerPreferenceService())

		val items = ItemLoaderService(FileService(properties)).loadAllItems()

		val numberOfTributes = min(24, (2 * floor(abs(tributes.size / 2.0))).toInt())

		val game = HungerGame(tributes.take(numberOfTributes), seed, deadlinessFactor = DeadlinessFactor.NORMAL)
		game.setEvents(eventLoaderService.loadAllEvents())
		game.setArenaEvents(eventLoaderService.loadAllArenaEvents())
		game.setItems(items)

		return game

	}

	private fun getMockProperties(): BotProperties {
		val properties = mockk<BotProperties>()
		every { properties.getPathProperty("workingDirectory") } returns Paths.get("")
		return properties
	}

}
