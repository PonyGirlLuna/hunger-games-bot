package net.krazyweb.hungergames

import io.mockk.every
import io.mockk.mockk
import net.krazyweb.hungergames.data.DeathType
import net.krazyweb.hungergames.data.Tribute
import net.krazyweb.hungergames.game.ArenaData
import net.krazyweb.hungergames.game.ItemEventType
import net.krazyweb.hungergames.services.EventFormatException
import net.krazyweb.hungergames.services.EventLoaderService
import net.krazyweb.hungergames.services.FileService
import net.krazyweb.hungergames.services.ItemLoaderService
import net.krazyweb.util.BotProperties
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.test.assertEquals
import kotlin.test.assertNull
import kotlin.test.assertTrue

class EventLoaderServiceTest : BotTest() {

	private val log: Logger = LogManager.getLogger(EventLoaderServiceTest::class.java)

	@Test
	fun `Event service sanity test`(@TempDir directory: Path) {

		copyTestEventsToFolder(directory)

		val properties = getMockedProperties(directory)
		val eventLoaderService = EventLoaderService(FileService(properties), getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		log.info("Loaded ${events.size} events from test directory")

		assertTrue { events.isNotEmpty() }

	}

	@Test
	fun `Event service loads all production data successfully`() {

		val properties = getMockedProperties(Paths.get(""))
		val eventLoaderService = EventLoaderService(FileService(properties), getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		log.info("Loaded ${events.size} events from production data directory")

		assertTrue { events.isNotEmpty() }

	}

	@Test
	fun `All production events have at least one valid item`() {

		val properties = getMockedProperties(Paths.get(""))
		val eventLoaderService = EventLoaderService(FileService(properties), getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		log.info("Loaded ${events.size} events from production data directory")

		val allItemTags = events.flatMap { it.itemReferences.flatMap { itemReference -> itemReference.tags } }.distinct().filter { it != "any" }

		val itemLoaderService = ItemLoaderService(FileService(properties))
		val items = itemLoaderService.loadAllItems()

		allItemTags.forEach {
			assertTrue(items.any { item -> it in item.tags }, "At least one item with tag '$it' is required, but one was not found.")
		}

	}

	@Test
	fun `All production arena events meet minimum requirements`() {

		val properties = getMockedProperties(Paths.get(""))
		val eventLoaderService = EventLoaderService(FileService(properties), getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		log.info("Loaded ${events.size} events from production data directory")

		val arenaEvents = eventLoaderService.loadAllArenaEvents()

		arenaEvents.forEach { arenaEvent ->

			val arenaData = ArenaData(1, "day", arenaEvent)
			val tribute = listOf(Tribute("1", 1L))

			assertTrue(events.filter { it.event == arenaEvent.name }.any { it.canRunEvent(tribute, fatal = false, itemEventType = ItemEventType.NONE, arenaData = arenaData) }, "Arena event '$arenaEvent' needs at least one non-fatal event with a single tribute.")

		}

	}

	@Test
	fun `Event service loads a default event`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		assertEquals("any", events.first().timeOfDay)
		assertEquals(listOf(), events.first().killers)
		assertEquals(listOf(), events.first().killed)
		assertEquals(listOf(), events.first().itemReferences)
		assertEquals(listOf(), events.first().inventoryChanges)
		assertNull(events.first().event)

	}

	@Test
	fun `Event service time of day loads correct values`(@TempDir directory: Path) {

		val fileService = getMockFileService()

		listOf("day", "night", "any").forEach {

			every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
				[{
					"message": "1",
					"timeOfDay": "$it"
				}]
			""".trimIndent())

			val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
			val events = eventLoaderService.loadAllEvents()

			assertEquals(it, events.first().timeOfDay)

		}

	}

	@Test
	fun `Event service loads time of day from field`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1",
				"timeOfDay": "day"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		assertEquals("day", events.first().timeOfDay)

	}

	@Test
	fun `Event service loads time of day from directory`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events", "day") } returns listOf("""
			[{
				"message": "1"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		assertEquals("day", events.first().timeOfDay)

	}

	@Test
	fun `Event service time of day from directory is overridden by field`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events", "day") } returns listOf("""
			[{
				"message": "1",
				"timeOfDay": "night"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		assertEquals("night", events.first().timeOfDay)

	}

	@Test
	fun `Event service loads arena event values`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events", "arena") } returns listOf("""
			[{
				"message": "1",
				"event": "The Bloodbath"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		assertEquals("The Bloodbath", events.first().event)

	}

	@Test
	fun `Event service loads single killer values`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1",
				"killers": 1,
				"killed": 1,
				"deathType": "murder"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		assertEquals(listOf(0), events.first().killers)

	}

	@Test
	fun `Event service loads array of killer values`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1 2",
				"killers": [1, 2],
				"killed": 1,
				"deathType": "murder"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		assertEquals(listOf(0, 1), events.first().killers)

	}

	@Test
	fun `Event service loads single killer`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1 2",
				"killer": 2,
				"killed": 1,
				"deathType": "murder"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		assertEquals(listOf(1), events.first().killers)

	}

	@Test
	fun `Event service loads single killed values`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1",
				"killed": 1,
				"deathType": "environmental"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		assertEquals(listOf(0), events.first().killed)

	}

	@Test
	fun `Event service loads all deathTypes`(@TempDir directory: Path) {

		for (type in listOf("murder", "accidental", "intentional", "environmental", "MURDER", "AcCiDeNTaL", "INTENTIONAL", "ENVIRONMENTAL")) {

			val fileService = getMockFileService()
			every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
				[{
					"message": "1 2",
					"killer": 2,
					"killed": 1,
					"deathType": "$type"
				}]
			""".trimIndent())

			val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
			val events = eventLoaderService.loadAllEvents()

			assertEquals(listOf(0), events.first().killed)

		}

	}

	@Test
	fun `Event service loads array of killed values`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1 2",
				"killed": [1, 2],
				"deathType": "environmental"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		assertEquals(listOf(0, 1), events.first().killed)

	}

	@Test
	fun `Event service requires killed when killer present`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1 2",
				"killers": 1,
				"deathType": "murder"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
		assertThrows(EventFormatException::class.java) { eventLoaderService.loadAllEvents() }

	}

	@Test
	fun `Event service loads deathTypes`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1 2",
				"killers": [1, 2],
				"killed": 1,
				"deathType": "murder"
			},{
				"message": "1 2",
				"killers": [1, 2],
				"killed": 1,
				"deathType": "accidental"
			},{
				"message": "1 2"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())
		val events = eventLoaderService.loadAllEvents()

		assertEquals(DeathType.MURDER, events.first().deathType)
		assertEquals(DeathType.ACCIDENTAL, events[1].deathType)
		assertNull(events[2].deathType)

	}

	@Test
	fun `Event service fails to load event without a message`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())

		expectException(EventFormatException::class.java, { eventLoaderService.loadAllEvents() }, "Event does not contain a 'message' field.")

	}

	@Test
	fun `Event service fails to load event with non-consecutive tribute ids`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1 3"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())

		expectException(EventFormatException::class.java, { eventLoaderService.loadAllEvents() }, "Tribute IDs must not skip numbers (ex: 1, 3, 4, 2; not 1, 3, 4, 5). They are currently [1, 3].")

	}

	@Test
	fun `Event service fails to load event with invalid time of day`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1",
				"timeOfDay": "lol"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())

		expectException(EventFormatException::class.java, { eventLoaderService.loadAllEvents() }, "'timeOfDay' is 'lol'. It must be one of ['day', 'night', 'any'] or be omitted.")

	}

	@Test
	fun `Event service fails to load event with more killers than tributes`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1",
				"killers": [1, 2],
				"deathType": "accidental"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())

		expectException(EventFormatException::class.java, { eventLoaderService.loadAllEvents() }, "'killers' references more tributes than are defined in the 'message' field (1).")

	}

	@Test
	fun `Event service fails to load event with more killed than tributes`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1",
				"killed": [1, 2],
				"deathType": "accidental"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())

		expectException(EventFormatException::class.java, { eventLoaderService.loadAllEvents() }, "'killed' references more tributes than are defined in the 'message' field (1).")

	}

	@Test
	fun `Event service fails to load event with unknown death type`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1",
				"killed": 1,
				"deathType": "????"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())

		expectException(EventFormatException::class.java, { eventLoaderService.loadAllEvents() }, "Unknown deathType. Valid values are: `murder`, `accidental`, `intentional`, `environmental`.")

	}

	@Test
	fun `Event service fails to load event with empty death type`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1",
				"killed": 1,
				"deathType": ""
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())

		expectException(EventFormatException::class.java, { eventLoaderService.loadAllEvents() }, "Unknown deathType. Valid values are: `murder`, `accidental`, `intentional`, `environmental`.")

	}

	@Test
	fun `Event service fails to load event with no deathType`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1",
				"killed": 1
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())

		expectException(EventFormatException::class.java, { eventLoaderService.loadAllEvents() }, "A deathType must be provided when tributes are killed.")

	}

	@Test
	fun `Event service fails to load event with deathType but no killed`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1",
				"deathType": "environmental"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())

		expectException(EventFormatException::class.java, { eventLoaderService.loadAllEvents() }, "A deathType has been provided, but no tributes are killed.")

	}

	@Test
	fun `Event service fails to load event with murder deathType but no killers`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1",
				"killed": 1,
				"deathType": "murder"
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())

		expectException(EventFormatException::class.java, { eventLoaderService.loadAllEvents() }, "deathType is murder, but no killers were specified.")

	}

	@Test
	fun `Event service fails to load event with more inventory changes than item references`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1 ${'$'}s1",
				"items": [
					{
						"id": 1,
						"owner": 1,
						"tags": ["any"]
					}
				],
				"inventory_changes": [
					{
						"item": 1,
						"from": 1,
						"to": 2
					},
					{
						"item": 2,
						"from": 2,
						"to": 1
					}
				]
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())

		expectException(EventFormatException::class.java, { eventLoaderService.loadAllEvents() }, "inventory_change[1] references items[1], but 'items' only contains 1 element(s).")

	}

	@Test
	fun `Event service fails to load event with missing item owner`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1 ${'$'}s1",
				"items": [
					{
						"id": 1,
						"tags": ["any"]
					}
				],
				"inventory_changes": [
					{
						"item": 1,
						"from": 1
					}
				]
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())

		expectException(EventFormatException::class.java, { eventLoaderService.loadAllEvents() }, "inventory_change[0] requires that Tribute 1 owns items[0], but items[0] is missing an 'owner' field.")

	}

	@Test
	fun `Event service fails to load event in which an item is given to its owner`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1 ${'$'}s1",
				"items": [
					{
						"id": 1,
						"owner": 1,
						"tags": ["any"]
					}
				],
				"inventory_changes": [
					{
						"item": 1,
						"to": 1
					}
				]
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())

		expectException(EventFormatException::class.java, { eventLoaderService.loadAllEvents() }, "inventory_change[0] gives tribute 1 their own item! (Don't do this <3)")

	}

	@Test
	fun `Event service fails to load event that has an item reference but none in the message field`(@TempDir directory: Path) {

		val fileService = getMockFileService()
		every { fileService.readAllFromDirectory("data", "events") } returns listOf("""
			[{
				"message": "1",
				"items": [
					{
						"id": 1,
						"owner": 1,
						"tags": ["any"]
					}
				]
			}]
		""".trimIndent())

		val eventLoaderService = EventLoaderService(fileService, getMockPronounService(), getMockPlayerPreferenceService())

		expectException(EventFormatException::class.java, { eventLoaderService.loadAllEvents() }, "Event contains a reference to one or more items, but no item references were included in the message field.")

	}

	private fun getMockedProperties(directory: Path): BotProperties {

		val properties = mockk<BotProperties>()
		every { properties.getPathProperty("workingDirectory") } returns directory

		return properties

	}

}
