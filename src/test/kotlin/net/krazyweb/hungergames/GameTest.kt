package net.krazyweb.hungergames

import io.mockk.every
import io.mockk.mockk
import net.krazyweb.hungergames.data.Tribute
import net.krazyweb.hungergames.data.messages.*
import net.krazyweb.hungergames.game.DeadlinessFactor
import net.krazyweb.hungergames.game.HungerGame
import net.krazyweb.hungergames.services.EventLoaderService
import net.krazyweb.hungergames.services.FileService
import net.krazyweb.hungergames.services.ItemLoaderService
import net.krazyweb.util.BotProperties
import nl.jqno.equalsverifier.EqualsVerifier
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.RepeatedTest
import org.junit.jupiter.api.RepetitionInfo
import org.junit.jupiter.api.Test
import java.nio.file.Paths
import kotlin.math.abs
import kotlin.math.floor
import kotlin.math.min
import kotlin.random.Random
import kotlin.test.*

class GameTest : BotTest() {

	@Test
	fun `An error is thrown for not having loaded any events`() {

		val game = getAGame()
		game.setEvents(listOf())

		assertThrows(IllegalStateException::class.java) { game.start() }

	}

	@Test
	fun `An error is thrown for not having loaded any tributes`() {

		val properties = getMockProperties()
		val events = EventLoaderService(FileService(properties), getMockPronounService(), getMockPlayerPreferenceService()).loadAllEvents()

		val game = HungerGame(listOf(), deadlinessFactor = DeadlinessFactor.TWI_PLS_NO)
		game.setEvents(events)

		assertThrows(IllegalStateException::class.java) { game.start() }

	}

	@Test
	fun `An error is thrown for not having an even number of tributes`() {

		val properties = getMockProperties()
		val tributes = getTributes()
		val events = EventLoaderService(FileService(properties), getMockPronounService(), getMockPlayerPreferenceService()).loadAllEvents()

		val numberOfTributes = 3

		val game = HungerGame(tributes.take(numberOfTributes), deadlinessFactor = DeadlinessFactor.TWI_PLS_NO)
		game.setEvents(events)

		assertThrows(IllegalStateException::class.java) { game.start() }

	}

	@RepeatedTest(100)
	fun `The game finishes`(info: RepetitionInfo) {

		val game = getAGame()

		var gameFinished = false

		game.addSubscriber(object : Subscriber {
			override fun call(message: Message) {
				when (message) {
					is GameOverMessage -> {
						gameFinished = true
					}
				}
			}
		})

		game.start()

		var attempts = 500000

		while (!gameFinished && attempts-- > 0) {
			game.next()
		}

		assertTrue(gameFinished, "Game ${info.currentRepetition} (${game.seed})")

	}

	@RepeatedTest(100)
	fun `The game doesn't try to continue when finished and it's requested to`(info: RepetitionInfo) {

		val game = getAGame()

		var gameFinished = false
		var turnCount = 0

		game.addSubscriber(object : Subscriber {
			override fun call(message: Message) {
				when (message) {
					is GameOverMessage -> {
						gameFinished = true
						turnCount = game.moveNumber
					}
					else -> {
						if (gameFinished) {
							throw RuntimeException("Game is finished. No more events should be received, yet one was! ${game.seed}")
						}
					}
				}
			}
		})

		game.start()

		var attempts = 500000

		while (attempts-- > 0) {
			game.next()
		}

		assertEquals(turnCount, game.moveNumber)

	}

	@RepeatedTest(100)
	fun `Every participant has an event in each phase`(info: RepetitionInfo) {

		val game = getAGame()
		val tributes = game.tributes

		var gameFinished = false
		val tributesNeedingTurns = mutableListOf<Tribute>()

		game.addSubscriber(object : Subscriber {
			override fun call(message: Message) {
				when (message) {
					is NonFatalEventMessage -> {
						tributesNeedingTurns -= message.tributes
					}
					is FatalEventMessage -> {
						tributesNeedingTurns -= message.tributes
					}
					is TimeChangeMessage, is ArenaEventMessage -> {
						assertTrue(tributesNeedingTurns.isEmpty(), "$tributesNeedingTurns still need turns -- Test run ${info.currentRepetition} -- Seed (${game.seed})")
						tributesNeedingTurns += tributes.filter { it.alive }
					}
					is GameOverMessage -> {
						gameFinished = true
					}
				}
			}
		})

		game.start()

		while (!gameFinished) {
			game.next()
		}

	}

	@RepeatedTest(100)
	fun `Games are repeatable with the same seed`(info: RepetitionInfo) {

		val seed = System.currentTimeMillis()

		val game1 = getAGame(seed)
		var game1Finished = false

		val game1Messages = mutableListOf<Message>()

		game1.addSubscriber(object : Subscriber {
			override fun call(message: Message) {
				game1Messages += message
				when (message) {
					is GameOverMessage -> {
						game1Finished = true
					}
				}
			}
		})

		game1.start()

		while (!game1Finished) {
			game1.next()
		}

		val game2 = getAGame(seed)
		var game2Finished = false

		val game2Messages = mutableListOf<Message>()

		game2.addSubscriber(object : Subscriber {
			override fun call(message: Message) {
				game2Messages += message
				when (message) {
					is GameOverMessage -> {
						game2Finished = true
					}
				}
			}
		})

		game2.start()

		while (!game2Finished) {
			game2.next()
		}

		game1Messages.forEachIndexed { index, message ->
			assertEquals(message, game2Messages[index])
		}

		assertEquals(game1, game2)

	}

	@RepeatedTest(100)
	fun `Games are fully replayable`(info: RepetitionInfo) {

		val game = getAGame()
		var gameFinished = false

		val gameMessages = mutableListOf<Message>()

		game.addSubscriber(object : Subscriber {
			override fun call(message: Message) {
				gameMessages += message
				when (message) {
					is GameOverMessage -> {
						gameFinished = true
					}
				}
			}
		})

		game.start()

		while (!gameFinished) {
			game.next()
		}

		gameFinished = false
		val firstRunMessages = gameMessages.toList()
		val firstRunDistricts = game.getDistricts().toList()
		gameMessages.clear()

		game.replay()

		firstRunMessages.forEachIndexed { index, message ->
			assertEquals(message, gameMessages[index])
		}

		assertEquals(firstRunDistricts, game.getDistricts())

	}

	@RepeatedTest(100)
	fun `Games can be partially replayed and resumed to completion`(info: RepetitionInfo) {

		val game = getAGame()

		var step = 0
		game.start()

		while (!game.isFinished() && step++ < 10) {
			game.next()
		}

		assertFalse(game.isFinished())

		game.replay()

		while (!game.isFinished()) {
			game.next()
		}

	}

	@Test
	fun `Subscriber is added`() {

		var receivedMessage: Message? = null

		val subscriber = object : Subscriber {
			override fun call(message: Message) {
				receivedMessage = message
			}
		}

		val game = getAGame()
		game.addSubscriber(subscriber)

		game.next()

		assertNotNull(receivedMessage)

	}

	@Test
	fun `Subscriber is removed`() {

		var receivedMessage: Message? = null

		val subscriber = object : Subscriber {
			override fun call(message: Message) {
				receivedMessage = message
			}
		}

		val game = getAGame()
		game.addSubscriber(subscriber)
		game.removeSubscriber(subscriber)

		game.next()

		assertNull(receivedMessage)

	}

	@Test
	fun `equals method is actually equal`() {

		EqualsVerifier
				.simple()
				.forClass(HungerGame::class.java)
				.withPrefabValues(Tribute::class.java, Tribute("1", 1L), Tribute("2", 2L))
				.withNonnullFields("events", "arenaEvents", "items", "timeHandler", "time", "tributesNeedingMovesThisRound", "tributesThatDiedToday", "deadlinessFactor", "arenaEventsUsed")
				.withIgnoredFields("log", "random", "messageSubscribers", "processing", "timeHandler")
				.verify()

	}

	private fun getAGame(seed: Long = System.currentTimeMillis()): HungerGame {

		val properties = getMockProperties()
		val tributes = getTributes()
		val eventLoaderService = EventLoaderService(FileService(properties), getMockPronounService(), getMockPlayerPreferenceService())
		val items = ItemLoaderService(FileService(properties)).loadAllItems()

		val numberOfTributes = min(24, (2 * floor(abs(tributes.size / 2.0))).toInt())

		val game = HungerGame(tributes.take(numberOfTributes), seed, deadlinessFactor = DeadlinessFactor.values().random(Random(seed)))
		game.setEvents(eventLoaderService.loadAllEvents())
		game.setArenaEvents(eventLoaderService.loadAllArenaEvents())
		game.setItems(items)

		return game

	}

	private fun getMockProperties(): BotProperties {
		val properties = mockk<BotProperties>()
		every { properties.getPathProperty("workingDirectory") } returns Paths.get("")
		return properties
	}

}
