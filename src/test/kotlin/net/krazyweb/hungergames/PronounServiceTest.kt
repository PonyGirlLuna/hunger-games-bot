package net.krazyweb.hungergames

import io.mockk.every
import io.mockk.mockk
import net.krazyweb.hungergames.data.Conjugation
import net.krazyweb.hungergames.data.Pronouns
import net.krazyweb.hungergames.services.PronounService
import net.krazyweb.util.BotProperties
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junitpioneer.jupiter.TempDir
import org.junitpioneer.jupiter.TempDirectoryExtension
import java.io.FileNotFoundException
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.test.assertEquals

class PronounServiceTest : BotTest() {

	private val pronounsFileName = "pronouns.json"

	@Test
	@ExtendWith(TempDirectoryExtension::class)
	fun `Pronoun service gets all pronoun sets`(@TempDir directory: Path) {

		val properties = preparePronounsTempFile(directory)

		val expected = listOf(
				Pronouns("he", "he", "him", "his", "his", "himself", Conjugation.SINGULAR),
				Pronouns("she", "she", "her", "her", "hers", "herself", Conjugation.SINGULAR),
				Pronouns("they", "they", "them", "their", "theirs", "themselves", Conjugation.PLURAL),
				Pronouns("🌮", "🌮", "🌮", "🌮", "🌮", "🌮", Conjugation.SINGULAR, selectable = false),
				Pronouns("team_they", "they", "them", "their", "theirs", "themselves", Conjugation.SINGULAR, selectable = false)
		)

		val pronounService = PronounService(properties)

		assertEquals(expected, pronounService.getPronounList())

	}

	@Test
	@ExtendWith(TempDirectoryExtension::class)
	fun `Pronoun service gets the correct pronoun set for a given nominative pronoun`(@TempDir directory: Path) {

		val properties = preparePronounsTempFile(directory)

		val expected = Pronouns("he", "he", "him", "his", "his", "himself", Conjugation.SINGULAR)

		val pronounService = PronounService(properties)

		assertEquals(expected, pronounService.getPronouns("he"))

	}

	@Test
	@ExtendWith(TempDirectoryExtension::class)
	fun `Pronoun service works with emotes for some absurd reason`(@TempDir directory: Path) {

		val properties = preparePronounsTempFile(directory)

		val expected = Pronouns("🌮", "🌮", "🌮", "🌮", "🌮", "🌮", Conjugation.SINGULAR, selectable = false)

		val pronounService = PronounService(properties)

		assertEquals(expected, pronounService.getPronouns("\uD83C\uDF2E"))

	}

	@Test
	fun `Pronoun service fails to find pronouns file`() {

		val properties = mockk<BotProperties>()
		every { properties.getPathProperty("workingDirectory") } returns Paths.get("this-should-be-invalid")

		val pronounService = PronounService(properties)

		assertThrows(FileNotFoundException::class.java) { pronounService.getPronouns("watermelon") }

	}

	@Test
	@ExtendWith(TempDirectoryExtension::class)
	fun `Pronoun service correctly fails to find an invalid pronoun set`(@TempDir directory: Path) {

		val properties = preparePronounsTempFile(directory)

		val pronounService = PronounService(properties)
		assertThrows(IllegalArgumentException::class.java) { pronounService.getPronouns("watermelon") }

	}

	private fun preparePronounsTempFile(directory: Path): BotProperties {

		Files.write(directory.resolve(pronounsFileName), readTestResource(pronounsFileName).toByteArray())

		val properties = mockk<BotProperties>()
		every { properties.getPathProperty("workingDirectory") } returns directory

		return properties

	}

}
