package net.krazyweb.hungergames

import io.mockk.every
import io.mockk.mockk
import net.krazyweb.hungergames.data.PlayerPreferenceKey
import net.krazyweb.hungergames.services.PlayerPreferenceService
import net.krazyweb.util.BotProperties
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.extension.ExtendWith
import org.junitpioneer.jupiter.TempDir
import org.junitpioneer.jupiter.TempDirectoryExtension
import java.nio.file.Files
import java.nio.file.Path
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class PlayerPreferenceServiceTest : BotTest() {

	private val preferencesFileName = "preferences.json"

	@Test
	@ExtendWith(TempDirectoryExtension::class)
	fun `Preference service gets defaults for empty file`(@TempDir directory: Path) {

		val properties = preparePreferencesTempFile(directory, "preferences-empty.json")

		val playerPreferencesService = PlayerPreferenceService(properties)

		assertEquals("they", playerPreferencesService.get(getMockUser(), PlayerPreferenceKey.PRONOUNS))

	}

	@Test
	@ExtendWith(TempDirectoryExtension::class)
	fun `Preference service saves a new preference`(@TempDir directory: Path) {

		val properties = preparePreferencesTempFile(directory, "preferences-empty.json")

		val playerPreferencesService = PlayerPreferenceService(properties)
		val user = getMockUser(1)

		playerPreferencesService.save(user, PlayerPreferenceKey.PRONOUNS, "they")
		assertEquals("they", playerPreferencesService.get(user, PlayerPreferenceKey.PRONOUNS))

	}

	@Test
	@ExtendWith(TempDirectoryExtension::class)
	fun `Preference service has a preference for user`(@TempDir directory: Path) {

		val properties = preparePreferencesTempFile(directory, "preferences.json")

		val playerPreferencesService = PlayerPreferenceService(properties)
		val user = getMockUser(0)

		assertTrue { playerPreferencesService.hasPreferenceForUser(user, PlayerPreferenceKey.PRONOUNS) }

	}

	@Test
	@ExtendWith(TempDirectoryExtension::class)
	fun `Preference service does not have a preference for user`(@TempDir directory: Path) {

		val properties = preparePreferencesTempFile(directory, "preferences-empty.json")

		val playerPreferencesService = PlayerPreferenceService(properties)
		val user = getMockUser(1)

		assertFalse { playerPreferencesService.hasPreferenceForUser(user, PlayerPreferenceKey.PRONOUNS) }

	}

	@Test
	@ExtendWith(TempDirectoryExtension::class)
	fun `Preference service creates new preference file if one does not exist`(@TempDir directory: Path) {

		val properties = mockk<BotProperties>()
		every { properties.getPathProperty("workingDirectory") } returns directory

		val playerPreferencesService = PlayerPreferenceService(properties)
		val user = getMockUser(1)

		assertFalse { playerPreferencesService.hasPreferenceForUser(user, PlayerPreferenceKey.PRONOUNS) }

	}

	@Test
	@ExtendWith(TempDirectoryExtension::class)
	fun `Preference service creates new preference file if one is empty`(@TempDir directory: Path) {

		Files.write(directory.resolve(preferencesFileName), "".toByteArray())

		val properties = mockk<BotProperties>()
		every { properties.getPathProperty("workingDirectory") } returns directory

		val playerPreferencesService = PlayerPreferenceService(properties)
		val user = getMockUser(1)

		assertFalse { playerPreferencesService.hasPreferenceForUser(user, PlayerPreferenceKey.PRONOUNS) }

	}

	@Test
	@ExtendWith(TempDirectoryExtension::class)
	fun `Preference service gets defaults for user with no saved preferences`(@TempDir directory: Path) {

		val properties = preparePreferencesTempFile(directory, "preferences.json")

		val playerPreferencesService = PlayerPreferenceService(properties)

		assertEquals("they", playerPreferencesService.get(getMockUser(99999), PlayerPreferenceKey.PRONOUNS))

	}

	@Test
	@ExtendWith(TempDirectoryExtension::class)
	fun `Preference service gets a saved preference`(@TempDir directory: Path) {

		val properties = preparePreferencesTempFile(directory, "preferences.json")

		val playerPreferencesService = PlayerPreferenceService(properties)

		assertEquals("he", playerPreferencesService.get(getMockUser(), PlayerPreferenceKey.PRONOUNS))

	}

	@Test
	@ExtendWith(TempDirectoryExtension::class)
	fun `Preference service gets defaults for missing preference for an existing user`(@TempDir directory: Path) {

		val properties = preparePreferencesTempFile(directory, "preferences.json")

		val playerPreferencesService = PlayerPreferenceService(properties)

		assertEquals("they", playerPreferencesService.get(getMockUser(1), PlayerPreferenceKey.PRONOUNS))

	}

	@Test
	@ExtendWith(TempDirectoryExtension::class)
	fun `Preference service gets a saved preference containing an emote`(@TempDir directory: Path) {

		val properties = preparePreferencesTempFile(directory, "preferences.json")

		val playerPreferencesService = PlayerPreferenceService(properties)

		assertEquals("🌮", playerPreferencesService.get(getMockUser(2), PlayerPreferenceKey.PRONOUNS))

	}

	@Test
	@ExtendWith(TempDirectoryExtension::class)
	fun `Preference service deletes a saved preference`(@TempDir directory: Path) {

		val properties = preparePreferencesTempFile(directory, "preferences.json")

		val playerPreferencesService = PlayerPreferenceService(properties)

		assertEquals("🌮", playerPreferencesService.get(getMockUser(2), PlayerPreferenceKey.PRONOUNS))
		playerPreferencesService.delete(getMockUser(2), PlayerPreferenceKey.PRONOUNS)
		assertEquals("they", playerPreferencesService.get(getMockUser(2), PlayerPreferenceKey.PRONOUNS))

	}

	@Test
	@ExtendWith(TempDirectoryExtension::class)
	fun `Preference service fails silently when a non-existent preference is deleted`(@TempDir directory: Path) {

		val properties = preparePreferencesTempFile(directory, "preferences.json")

		val playerPreferencesService = PlayerPreferenceService(properties)

		assertDoesNotThrow { playerPreferencesService.delete(getMockUser(1), PlayerPreferenceKey.PRONOUNS) }
		assertEquals("they", playerPreferencesService.get(getMockUser(1), PlayerPreferenceKey.PRONOUNS))

	}

	@Test
	@ExtendWith(TempDirectoryExtension::class)
	fun `Preference service fails silently when a non-existent user's preference is deleted`(@TempDir directory: Path) {

		val properties = preparePreferencesTempFile(directory, "preferences.json")

		val playerPreferencesService = PlayerPreferenceService(properties)

		assertDoesNotThrow { playerPreferencesService.delete(getMockUser(99999), PlayerPreferenceKey.PRONOUNS) }

	}

	@Test
	@ExtendWith(TempDirectoryExtension::class)
	fun `Preference service gets all instances of a set preference`(@TempDir directory: Path) {

		val properties = preparePreferencesTempFile(directory, "preferences.json")

		val playerPreferencesService = PlayerPreferenceService(properties)
		val pingPreferences = playerPreferencesService.getAll(PlayerPreferenceKey.PING_ON_START)

		pingPreferences.forEach {
			assert(it.key in listOf(100000000000000001, 100000000000000002))
		}

	}

	@Test
	@ExtendWith(TempDirectoryExtension::class)
	fun `Preference service deletes the correct preference data only`(@TempDir directory: Path) {

		val properties = preparePreferencesTempFile(directory, "preferences.json")

		val playerPreferencesService = PlayerPreferenceService(properties)

		playerPreferencesService.delete(getMockUser(2), PlayerPreferenceKey.PRONOUNS)
		assertEquals("he", playerPreferencesService.get(getMockUser(), PlayerPreferenceKey.PRONOUNS))
		assertEquals("they", playerPreferencesService.get(getMockUser(1), PlayerPreferenceKey.PRONOUNS))
		assertEquals("they", playerPreferencesService.get(getMockUser(2), PlayerPreferenceKey.PRONOUNS))

	}

	@Test
	@ExtendWith(TempDirectoryExtension::class)
	fun `Preference service saves a preference`(@TempDir directory: Path) {

		val properties = preparePreferencesTempFile(directory, "preferences.json")

		val playerPreferencesService = PlayerPreferenceService(properties)
		val user = getMockUser(1)

		assertEquals("they", playerPreferencesService.get(user, PlayerPreferenceKey.PRONOUNS))
		playerPreferencesService.save(user, PlayerPreferenceKey.PRONOUNS, "he")
		assertEquals("he", playerPreferencesService.get(user, PlayerPreferenceKey.PRONOUNS))

	}

	@Test
	@ExtendWith(TempDirectoryExtension::class)
	fun `Preference service overwrites a preference`(@TempDir directory: Path) {

		val properties = preparePreferencesTempFile(directory, "preferences.json")

		val playerPreferencesService = PlayerPreferenceService(properties)
		val user = getMockUser()

		assertEquals("he", playerPreferencesService.get(user, PlayerPreferenceKey.PRONOUNS))
		playerPreferencesService.save(user, PlayerPreferenceKey.PRONOUNS, "she")
		assertEquals("she", playerPreferencesService.get(user, PlayerPreferenceKey.PRONOUNS))

	}

	@Test
	@ExtendWith(TempDirectoryExtension::class)
	fun `Preference service saves a preference with an emote value`(@TempDir directory: Path) {

		val properties = preparePreferencesTempFile(directory, "preferences.json")

		val playerPreferencesService = PlayerPreferenceService(properties)
		val user = getMockUser(1)

		assertEquals("they", playerPreferencesService.get(user, PlayerPreferenceKey.PRONOUNS))
		playerPreferencesService.save(user, PlayerPreferenceKey.PRONOUNS, "🌮")
		assertEquals("🌮", playerPreferencesService.get(user, PlayerPreferenceKey.PRONOUNS))

	}

	@Test
	@ExtendWith(TempDirectoryExtension::class)
	fun `Preference service saves the preference data to one user only`(@TempDir directory: Path) {

		val properties = preparePreferencesTempFile(directory, "preferences.json")

		val playerPreferencesService = PlayerPreferenceService(properties)

		playerPreferencesService.save(getMockUser(), PlayerPreferenceKey.PRONOUNS, "she")
		assertEquals("she", playerPreferencesService.get(getMockUser(), PlayerPreferenceKey.PRONOUNS))
		assertEquals("they", playerPreferencesService.get(getMockUser(1), PlayerPreferenceKey.PRONOUNS))
		assertEquals("🌮", playerPreferencesService.get(getMockUser(2), PlayerPreferenceKey.PRONOUNS))

	}

	@Test
	@ExtendWith(TempDirectoryExtension::class)
	fun `Preference service deletes the preference data for one user only`(@TempDir directory: Path) {

		val properties = preparePreferencesTempFile(directory, "preferences.json")

		val playerPreferencesService = PlayerPreferenceService(properties)

		playerPreferencesService.delete(getMockUser(), PlayerPreferenceKey.PRONOUNS)
		assertFalse(playerPreferencesService.hasPreferenceForUser(getMockUser(), PlayerPreferenceKey.PRONOUNS))
		assertEquals("they", playerPreferencesService.get(getMockUser(1), PlayerPreferenceKey.PRONOUNS))
		assertEquals("🌮", playerPreferencesService.get(getMockUser(2), PlayerPreferenceKey.PRONOUNS))

	}

	@Test
	@ExtendWith(TempDirectoryExtension::class)
	fun `Preference service saves the correct preference for a user only`(@TempDir directory: Path) {

		val properties = preparePreferencesTempFile(directory, "preferences.json")

		val playerPreferencesService = PlayerPreferenceService(properties)

		playerPreferencesService.save(getMockUser(), PlayerPreferenceKey.PING_ON_START, "true")
		assertEquals("he", playerPreferencesService.get(getMockUser(), PlayerPreferenceKey.PRONOUNS))
		assertEquals("true", playerPreferencesService.get(getMockUser(), PlayerPreferenceKey.PING_ON_START))

	}

	@Test
	@ExtendWith(TempDirectoryExtension::class)
	fun `Preference service deletes the correct preference for a user only`(@TempDir directory: Path) {

		val properties = preparePreferencesTempFile(directory, "preferences.json")

		val playerPreferencesService = PlayerPreferenceService(properties)

		playerPreferencesService.save(getMockUser(), PlayerPreferenceKey.PING_ON_START, "true")
		playerPreferencesService.delete(getMockUser(), PlayerPreferenceKey.PRONOUNS)
		assertFalse(playerPreferencesService.hasPreferenceForUser(getMockUser(), PlayerPreferenceKey.PRONOUNS))
		assertEquals("true", playerPreferencesService.get(getMockUser(), PlayerPreferenceKey.PING_ON_START))

	}

	private fun getMockUser(userNumber: Int = 0): Long {
		return 100000000000000000 + userNumber
	}

	private fun preparePreferencesTempFile(directory: Path, preferencesFileToUse: String): BotProperties {

		Files.write(directory.resolve(preferencesFileName), readTestResource(preferencesFileToUse).toByteArray())

		val properties = mockk<BotProperties>()
		every { properties.getPathProperty("workingDirectory") } returns directory

		return properties

	}

}
